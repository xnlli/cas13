#!/usr/bin/env Rscript



###### set variables; load libs #####################
.libPaths(new="~/R/x86_64-redhat-linux-gnu-library/3.5")
suppressMessages(library(Biobase, quietly = T))
suppressMessages(library(reshape2, quietly = T))
suppressMessages(library(ggplot2, quietly = T))
suppressMessages(library(gridExtra, quietly = T))
suppressMessages(library(grid, quietly = T))
suppressMessages(library(NMF, quietly = T))
suppressMessages(library(RobustRankAggreg, quietly = T))
suppressMessages(library(DelayedArray, quietly = T))
suppressMessages(library(sva, quietly = T))
suppressMessages(library(limma,quietly = T))



###### sub-routines #################################
GetName <- function(x,y=2){  return(gsub("wd_","",strsplit( paste0(strsplit(x, split = "/")[[1]][c(y,y+1)], collapse = "-") , split = "\\." )[[1]][1])) }

gm_mean = function(x, na.rm=TRUE, zero.propagate = FALSE){
  if(any(x < 0, na.rm = TRUE)){
    return(NaN)
  }
  if(zero.propagate){
    if(any(x == 0, na.rm = TRUE)){
      return(0)
    }
    exp(mean(log(x), na.rm = na.rm))
  } else {
    exp(sum(log(x[x > 0]), na.rm=na.rm) / length(x))
  }
}

NormalizeLikeDEseq <- function(x,ref = PseudoRef){
  if (!(all(names(ref) == rownames(mat)))){
    stop()
  }
  else if(all(names(ref) == rownames(mat))){
    
    ratios <- apply(mat, MARGIN = 2, function(m){m/ref})
    
    ColMedians <- apply(ratios, MARGIN = 2, median)
    
    norm = function(s,m){s/m}
    
    mat.norm <- mat
    
    for (i in 1:length(ColMedians)){
      mat.norm[,i] <- norm(s = mat[,i], m = ColMedians[i])
    }
    
    l <- list()
    l[[1]] <- mat
    l[[2]] <- mat.norm
    l[[3]] <- ColMedians
    names(l) <- c("input_counts","normalized_counts","scaling_factors")
    
    return(l)
  }
  
}

BatchRemove <- function(mat, batchMat, log2trans=FALSE, positive = FALSE){
  if(class(mat)=="character" && file.exists(mat)){
    mat = read.table(mat, sep = "\t", header = TRUE, stringsAsFactors = FALSE)
  }
  if(class(batchMat)=="character" && file.exists(batchMat)){
    batchMat = read.table(batchMat, sep = "\t", header = TRUE, stringsAsFactors = FALSE)
  }
  #requireNamespace("sva")
  mat = as.matrix(mat)
  if(mode(mat)!="numeric") stop("Numeric data matrix is needed!")
  batch = as.matrix(batchMat)
  rownames(batch) = batch[,1]
  ## load batch matrix
  index=intersect(batch[,1], colnames(mat))
  if(length(index)<2) stop("Less than two samples found in data matrix.")
  dt=mat[,index]
  if(log2trans) dt = log(dt+1)
  ##
  tmp=dt
  var0=numeric()
  for (i in unique(batch[,2])){
    temp=as.character(batch[batch[,2]==i,1])
    if (length(temp)>=2){
      temp2=tmp[,temp]
      var.data.tmp <- apply(temp2, 1,function(x) var(x))
      var.data.tmp0=which(var.data.tmp==0)
      var0=c(var0,var.data.tmp0)
    }
  }
  tmp2 = tmp
  if(length(var0)>0) tmp2=tmp[setdiff(1:nrow(tmp),var0),]
  if(ncol(batch)>2){
    mod = as.data.frame(batch[,3:ncol(batch)])
    mod = model.matrix(~., data=mod)
  }else mod = NULL
  if(length(unique(batch[,2]))<2){
    res = tmp2
  }else{res <- sva::ComBat(tmp2, batch = batch[,2], mod = mod)}
  if(positive) res[res<0] = 0
  if(length(setdiff(colnames(mat), colnames(res)))>0){
    res=cbind(mat[setdiff(1:nrow(tmp),var0), setdiff(colnames(mat),colnames(res))], res)
    colnames(res)[1:length(setdiff(colnames(mat), colnames(res)))] = setdiff(colnames(mat),colnames(res))
  }
  if(length(var0)>0){
    res = rbind(res, mat[var0, colnames(res)])
  }
  
  # dt2 = matrix(as.numeric(res[,index]), ncol = length(index))
  # colnames(dt2) = index
  # rownames(dt2) = rownames(res)
  dt2 = res[,index]
  pca1 = NULL
  pca2 = NULL
  p1 = NULL
  
  pca1 = prcomp(t(na.omit(dt)))$x[,1:2]
  pca2 = prcomp(t(na.omit(dt2)))$x[,1:2]
  
  gg1 = as.data.frame(pca1, stringsAsFactors=FALSE)
  gg1$col = batch[rownames(gg1),2]
  gg1$group = factor("Before batch removal", "Before batch removal")
  gg2 = as.data.frame(pca2,stringsAsFactors=FALSE)
  gg2$col = batch[rownames(gg2),2]
  gg2$group = factor("After batch removal", "After batch removal")
  if(ncol(batch)>2){
    gg1$shape = as.character(batch[rownames(gg1),3])
    gg2$shape = batch[rownames(gg2),3]
  }else{ gg1$shape = "NA"; gg2$shape = "NA" }
  gg = rbind.data.frame(gg1, gg2)
  
  #====plot PC1 and PC2=====
  p1 = ggplot(gg, aes(x=PC1, y=PC2, color=col, shape=shape))
  p1 = p1 + geom_point(size = 2)
  p1 = p1 + scale_color_discrete(name="Batch", breaks = unique(gg$col))
  #p1 = p1 + scale_shape_discrete(name="Batch", breaks = unique(gg$col))
  p1 = p1 + theme_bw(14)+theme(plot.title = element_text(hjust = 0.5,size=12))
  p1 = p1 + facet_grid(~group, switch = "y", scales="free")
  p1 = p1 + theme(legend.title=element_blank())
  
  
  
  pca1 = prcomp(t(na.omit(dt)))$x[,2:3]
  pca2 = prcomp(t(na.omit(dt2)))$x[,2:3]
  
  gg1 = as.data.frame(pca1, stringsAsFactors=FALSE)
  gg1$col = batch[rownames(gg1),2]
  gg1$group = factor("Before batch removal", "Before batch removal")
  gg2 = as.data.frame(pca2,stringsAsFactors=FALSE)
  gg2$col = batch[rownames(gg2),2]
  gg2$group = factor("After batch removal", "After batch removal")
  if(ncol(batch)>2){
    gg1$shape = as.character(batch[rownames(gg1),3])
    gg2$shape = batch[rownames(gg2),3]
  }else{ gg1$shape = "NA"; gg2$shape = "NA" }
  gg = rbind.data.frame(gg1, gg2)
  
  #====plot PC2 and PC3=====
  p2 = ggplot(gg)
  p2 = p2 + geom_point(aes(x=PC2, y=PC3, color=col, shape=shape),size = 2)
  p2 = p2 + scale_color_discrete(name="Batch", breaks = unique(gg$col))
  #p2 = p2 + scale_shape_discrete(name="Batch", breaks = unique(gg$col))
  p2 = p2 + theme_bw(14)+theme(plot.title = element_text(hjust = 0.5,size=12))
  p2 = p2 + facet_grid(~group, switch = "y", scales="free")
  p2 = p2 + theme(legend.title=element_blank())
  
  
  
  pca1 = prcomp(t(na.omit(dt)))$x[,3:4]
  pca2 = prcomp(t(na.omit(dt2)))$x[,3:4]
  
  gg1 = as.data.frame(pca1, stringsAsFactors=FALSE)
  gg1$col = batch[rownames(gg1),2]
  gg1$group = factor("Before batch removal", "Before batch removal")
  gg2 = as.data.frame(pca2,stringsAsFactors=FALSE)
  gg2$col = batch[rownames(gg2),2]
  gg2$group = factor("After batch removal", "After batch removal")
  if(ncol(batch)>2){
    gg1$shape = as.character(batch[rownames(gg1),3])
    gg2$shape = batch[rownames(gg2),3]
  }else{ gg1$shape = "NA"; gg2$shape = "NA" }
  gg = rbind.data.frame(gg1, gg2)
  
  #====plot PC3 and PC4=====
  p3 = ggplot(gg)
  p3 = p3 + geom_point(aes(x=PC3, y=PC4, color=col, shape=shape),size = 2)
  p3 = p3 + scale_color_discrete(name="Batch", breaks = unique(gg$col))
  #p3 = p3 + scale_shape_discrete(name="Batch", breaks = unique(gg$col))
  p3 = p3 + theme_bw(14)+theme(plot.title = element_text(hjust = 0.5,size=12))
  p3 = p3 + facet_grid(~group, switch = "y", scales="free")
  p3 = p3 + theme(legend.title=element_blank())
  
  ##====Clustering========
  # if(cluster){
  #   filename = file.path(outdir, paste0(prefix, "_hclust_batchremoval.pdf"))
  #   if(is.na(hclust.height) | is.na(hclust.width))
  #     pdf(filename, height=0.5*nrow(batch)+2, width=0.05*nrow(batch)+3)
  #   else
  #     pdf(filename, height=hclust.height, width=hclust.width)
  #   cc2 = cor(dt)
  #   cc=cor(dt2)
  #   if(ncol(batch)>2){
  #     hclustView(cc2, label_cols = batch[rownames(cc2),3], bar_cols = batch[rownames(cc2),2:ncol(batch)], main = "Before batch removal")
  #     hclustView(cc, label_cols = batch[rownames(cc),3], bar_cols = batch[rownames(cc),2:ncol(batch)], main = "After batch removal")
  #
  #   }else{
  #     hclustView(cc2, label_cols = batch[rownames(cc2),2], main = "Before batch removal")
  #     hclustView(cc, label_cols = batch[rownames(cc),2], main = "After batch removal")
  #   }
  #   dev.off()
  # }
  return(list(data=res, p1=p1, p2=p2, p3=p3))
}




Plot.pca <- function(x, NAME = NULL , LVLs = c("input" ,   "top" ,  "bot")){
  PC<-prcomp(log2(na.omit(x)))
  
  df_out_r <- as.data.frame(PC$rotation)
  df_out_r$feature <- row.names(df_out_r)
  df_out_r$batch <- sapply(df_out_r$feature,function(x){strsplit(x, split="_")[[1]][1]})
  df_out_r$bin <- sapply(df_out_r$feature,function(x){strsplit(x, split="_")[[1]][2]})
  
  df_out_r$batch = factor(df_out_r$batch , levels = names(table(df_out_r$batch)) )
  df_out_r$bin = factor(df_out_r$bin , levels = LVLs )
  
  max.x <- max(PC$rotation[,1])
  max.y <- max(PC$rotation[,2])
  min.x <- min(PC$rotation[,1])
  min.y <- min(PC$rotation[,2])
  
  ratio = abs(min.x - max.x)/abs(min.y - max.y) # get plot dimensions
  
  g1 <- ggplot(df_out_r,aes(x=PC1,y=PC2,color=batch, shape=bin  )) + geom_point(size=3)+theme_classic() + coord_fixed(ratio=ratio) + ggtitle(NAME)
  
  
  
  max.x <- max(PC$rotation[,2])
  max.y <- max(PC$rotation[,3])
  min.x <- min(PC$rotation[,2])
  min.y <- min(PC$rotation[,3])
  
  ratio = abs(min.x - max.x)/abs(min.y - max.y) # get plot dimensions
  
  g2 = ggplot(df_out_r,aes(x=PC2,y=PC3,color=batch, shape=bin  )) + geom_point(size=3)+theme_classic() + coord_fixed(ratio=ratio) + ggtitle(NAME)
  
  
  return(list(g1,g2))
  
  
}

# now updated, log2 instead of log
getCSscore <-function( query , ref ){
  return(log2(query/ref))
}


AssignCScore <- function(x){
  
  # initialize in and output
  mat = x
  mat.cs = mat
  
  # calculate CS in a quick and dirty way; could possibly be done nicer
  mat.cs[,which(colnames(mat.cs) == "R1_top")] <- getCSscore(query = mat[,which(colnames(mat) == "R1_top")], ref= mat[,which(colnames(mat) == "R1_input")] )
  mat.cs[,which(colnames(mat.cs) == "R2_top")] <- getCSscore(query = mat[,which(colnames(mat) == "R2_top")], ref= mat[,which(colnames(mat) == "R2_input")] )
  mat.cs[,which(colnames(mat.cs) == "R1_bot")] <- getCSscore(query = mat[,which(colnames(mat) == "R1_bot")], ref= mat[,which(colnames(mat) == "R1_input")] )
  mat.cs[,which(colnames(mat.cs) == "R2_bot")] <- getCSscore(query = mat[,which(colnames(mat) == "R2_bot")], ref= mat[,which(colnames(mat) == "R2_input")] )
  
  # remove inputs
  mat.cs <- mat.cs[,grep("input",colnames(mat.cs), invert = T)]
  
  # return CS matrix
  return(mat.cs)
  
}


GetType<-function(x){
  if(grepl("FirstOrder",x)){
    return("First Order")
  }
  else if (grepl("consecTriple",x)){
    return("Consecutive Triple")
  }
  else if (grepl("randomDouble",x)){
    return("Random Double")
  }
  else if (grepl("rc_",x)){
    return("Non-Targeting")
  }
  else if (grepl("consecDouble",x)){
    return("Consecutive Double")
  }
  else if (grepl("LengthVariant",x)){
    return("Length Variant")
  }
  else if (grepl("intron",x)){
    return("Intronic")
  }
  else if (grepl("Rev",x)){
    return("Reverse Complement")
  }
  else{
    return("Perfect Match")
  }
}

RunRRA <- function(x, NAME = NULL , info = INFO){
  mat.cs = x
  
  # older try, also works...
  #mat.Rank <- apply(mat.cs, MARGIN = 2, TransformToRank,  DECREASE = TRUE)
  # I found out later, that the rank() function exists....
  mat.rank <- apply(mat.cs, MARGIN = 2, GetRank,  DECREASE = FALSE)
  
  mat.rank.BIN1 <- mat.rank[,grep("top",colnames(mat.rank))]
  mat.rank.BIN2 <- mat.rank[,grep("bot",colnames(mat.rank))]

  mat.RRA.BIN1.des <-  apply(mat.rank.BIN1, MARGIN = 1,rhoScores)
  mat.RRA.BIN2.des <-  apply(mat.rank.BIN2, MARGIN = 1,rhoScores)

  
  
  
  mat.rank <- apply(mat.cs, MARGIN = 2, GetRank,  DECREASE = TRUE)
  
  mat.rank.BIN1 <- mat.rank[,grep("top",colnames(mat.rank))]
  mat.rank.BIN2 <- mat.rank[,grep("bot",colnames(mat.rank))]

  

  mat.RRA.BIN1.inc <-  apply(mat.rank.BIN1, MARGIN = 1,rhoScores)
  mat.RRA.BIN2.inc <-  apply(mat.rank.BIN2, MARGIN = 1,rhoScores)

  
  if (all(names(mat.RRA.BIN1.des) == names(mat.RRA.BIN1.inc)) &
      all(names(mat.RRA.BIN2.des) == names(mat.RRA.BIN2.inc)) ){
    

    RRA.BIN1 <- rowMin(cbind(mat.RRA.BIN1.des,mat.RRA.BIN1.inc))
    RRA.BIN2 <- rowMin(cbind(mat.RRA.BIN2.des,mat.RRA.BIN2.inc))

    
  }
  else{
    stop("guides are not in the correct order")
  }
  
  

  names(RRA.BIN1) <- names(mat.RRA.BIN1.des)
  names(RRA.BIN2) <- names(mat.RRA.BIN2.des)

  
  
  
  RRA <- cbind(RRA.BIN1,RRA.BIN2)
  colnames(RRA) <- c("p.Top","p.Bottom")
  RRA <- as.data.frame(RRA)
  RRA$logTop <- -log10(RRA$p.Top)
  RRA$logBottom <- -log10(RRA$p.Bottom)

  RRA$meanCS.Top <- rowMeans(as.matrix(mat.cs[,grep("top",colnames(mat.cs))]), na.rm = T)
  RRA$meanCS.Bottom <- rowMeans(as.matrix(mat.cs[,grep("bot",colnames(mat.cs))]), na.rm = T)

  
  RRA$medianCS.Top <- rowMedians(as.matrix(mat.cs[,grep("top",colnames(mat.cs))]), na.rm = T)
  RRA$medianCS.Bottom <- rowMedians(as.matrix(mat.cs[,grep("bot",colnames(mat.cs))]), na.rm = T)

  
  RRA$Guide <- sapply(rownames(RRA),GetType)
  RRA$Guide <- factor(RRA$Guide, levels = c("Perfect Match", "First Order", "Random Double","Length Variant",  "Intronic", "Reverse Complement", "Non-Targeting"))
  

  cbbPalette <- c( "#E69F00","#D55E00", "#009E73",  "#0072B2", "#56B4E9", "#252525", "#999999")
  
  g1 = ggplot(aes(y=RRA$logTop, x=RRA$meanCS.Top), data = RRA) + geom_point(aes(color = Guide), size = 0.5, alpha = 1) + xlab("log2(Top/Input)") + ylab("-log10(RRA)") +
    theme_classic() +
    ggtitle(NAME) +
    scale_color_manual(values=cbbPalette)+
    theme(plot.title = element_text(size=10))
  
  g2 = ggplot(aes(y=RRA$logBottom, x=RRA$meanCS.Bottom), data = RRA) + geom_point(aes(color = Guide), size = 0.5, alpha = 1) + xlab("log2(Bottom/Input)") + ylab("-log10(RRA)") +
    theme_classic() +
    ggtitle(NAME) +
    scale_color_manual(values=cbbPalette)+
    theme(plot.title = element_text(size=10))

    
  g3 = ggplot(data=RRA, aes( y = meanCS.Top, x = Guide,  fill = Guide)) + 
      geom_violin() +  
      geom_boxplot( fill = "white", width = 0.5, outlier.size = -1)+
      ggtitle(NAME) +
      theme_classic() +
      ylab("log2(Top/Input)")+
      xlab("")+
      geom_hline(yintercept = 0, colour = "#969696", linetype = "longdash", size = 1) +
      scale_fill_manual(values=cbbPalette)+
      theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.75))+
      theme(axis.text.x=element_blank(),
            axis.ticks.x=element_blank(),
            plot.title = element_text(size=10))
  
  g4 = ggplot(data=RRA, aes( y = meanCS.Bottom, x = Guide,  fill = Guide)) + 
    geom_violin() +  
    geom_boxplot( fill = "white", width = 0.5, outlier.size = -1)+
    ggtitle(NAME) +
    theme_classic() +
    ylab("log2(Bottom/Input)")+
    xlab("")+
    geom_hline(yintercept = 0, colour = "#969696", linetype = "longdash", size = 1) +
    scale_fill_manual(values=cbbPalette)+
    theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.75))+
    theme(axis.text.x=element_blank(),
          axis.ticks.x=element_blank(),
          plot.title = element_text(size=10))
  

 
  
  l <- list()
  l[[1]] <- RRA
  l[[2]] <- g1
  l[[3]] <- g2
  l[[4]] <- g3
  l[[5]] <- g4


  names(l) <- c("RRA_results", "Volcano_plot_mean_Top","Volcano_plot_mean_Bottom","Violine_plot_mean_Top","Violine_plot_mean_Bottom")
  return(l)
}

GetRank <- function(y, DECREASE = TRUE){
  if(DECREASE == T){
    n <- rank(-y , na.last = T, ties.method = "last") / length(y)
    
  }
  else{
    n <- rank(y , na.last = T, ties.method = "last") / length(y)
  }
  return(n)
}


Plot_9010 = function(df = COUNTS[[1]] , name = names(COUNTS)[1] ){
  Q = quantile(df$RawReadCount , probs = c(0.1,0.9))
  R = signif(Q[2]/Q[1],3)
  ggplot(data=df, aes(RawReadCount)) + 
    geom_histogram(binwidth = 5, colour="#252525", fill="#f0f0f0") + 
    ggtitle(name) +
    theme_classic() +
    geom_vline(xintercept = median(df$RawReadCount), colour = "#cb181d", linetype = "longdash", size = 1) +
    annotate( geom = "text", x = median(df$RawReadCount)+(median(df$RawReadCount)/1.25), y = 150, label = paste0("90/10 = ",R) )
}

Plot_CountsByCategory = function(df = COUNTS[[1]] , name = names(COUNTS)[1] ){
  
  ggplot(data=df, aes( y = RawReadCount, x = Type,  fill = Type)) + 
    geom_violin() +  
    geom_boxplot( fill = "white", width = 0.5, outlier.size = -1)+
    ggtitle(name) +
    theme_classic() +
    geom_hline(yintercept = median(df$RawReadCount), colour = "#969696", linetype = "longdash", size = 1) +
    theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.75))
}


Match_and_Count = function(lib, ref){
  
  m = match(as.character(ref),as.character(lib) )
  
  del = which(is.na(m) == T)
  if(length(del) > 0){
    zero = ref[del]
    ref = ref[-del]
    m = match(as.character(ref),as.character(lib) )
  }
  
  if( all( as.character(ref) == as.character(lib)[m])){
    df = as.data.frame(ref)
    colnames(df) = "Sequence"
    df$RawReadCount = as.numeric(as.character(sapply( names(lib)[m] , FUN = function(x){strsplit(x, split = "-")[[1]][2]})))
    if (length(del) > 0 ){
      zero.df = as.data.frame(zero)
      colnames(zero.df) = "Sequence"
      zero.df$RawReadCount = 0
      df = rbind.data.frame(df,zero.df)
    }
    df$Type = sapply(rownames(df),GetType)
    return(df)
  }
  else{
    stop("Exiting. Check sequence matching")
  }
}



########################## sub-routines end  ########

###### execute ######################################


# Get reference
ref.file = "./data/CD71_library_final.fa"
REFs = lapply(ref.file , FUN = function(x){  Biostrings::readDNAStringSet(filepath = x, format = "fasta") })
names(REFs) = gsub("_library_final.fa","",basename(ref.file))



#### generate raw count matrix

cat( paste0("\n Generate raw count matrix: ",date(),"\n"))

#load crRNA count results
fa.files <- list.files(path = "./data/CD71",  recursive = T, full.names = T,pattern = ".fa$")
Libs = lapply(fa.files , FUN = function(x){  Biostrings::readDNAStringSet(filepath = x, format = "fasta") })
names(Libs) <- sapply(fa.files , GetName, y=3)


# Sample naming 
ID_matrix = cbind(c( "CD71-F7",  "CD71-F8",  "CD71-F9","CD71-F10", "CD71-F11", "CD71-F12" ),
                  c("R1_top","R2_top","R1_bot","R2_bot","R1_input","R2_input"))
colnames(ID_matrix) = c("Barcodes","Sample")

#rename samples and order
for (i in 1:length(Libs)){
  idx = which( ID_matrix[,"Barcodes"] == names(Libs)[i]  )
  names(Libs)[i] = ID_matrix[,"Sample"][idx]
}
order <- c("R1_input","R2_input", "R1_top","R2_top",  "R1_bot","R2_bot")
Libs = Libs[order]


# match by sequence, names contains counts
COUNTS = list()
for ( i in 1:length(Libs)){
  COUNTS[[i]] = Match_and_Count(lib = Libs[[i]] , ref = REFs[[1]] )
}
names(COUNTS) = names(Libs)


# how many are not detected
# sapply(COUNTS , FUN = function(x){length(which(x$RawReadCount != 0))})
# sapply(COUNTS , FUN = function(x){sum(x$RawReadCount)})

# Plot 90/10
G = list()
for(i in 1:length(COUNTS)){
  G[[i]] = Plot_9010(df = COUNTS[[i]] , name = names(COUNTS)[i]) + xlim(c(0,1500))
}

pdf("./figures/QC_CD71_NinetyTen.pdf", width = length(G)*3, height = 3, useDingbats = F)
grid.arrange(arrangeGrob(grobs= G , ncol = length(G)) )
dev.off()




# Plot cnt dist by cat
G = list()
for(i in 1:length(COUNTS)){
  G[[i]] = Plot_CountsByCategory(df = COUNTS[[i]] , name = names(COUNTS)[i]) + ylim(0,1500)
}

pdf("./figures/QC_CD71_CountDistByCat.pdf", width = length(G)*4, height = 4, useDingbats = F)
grid.arrange(arrangeGrob(grobs= G , ncol = length(G)) )
dev.off()


# Get a set of guides that is present across all conditions
rn = rownames(COUNTS[[1]]) # initialize
for ( i in 1:length(COUNTS)){
  rn = intersect(rn, rownames(COUNTS[[i]]) ) # update iteratively
}

# transform into count matrix; the order is the same in all COUNTS list entries (Test if it works like this)
Test <- do.call(cbind, lapply(COUNTS, function(x){  x$rn = rownames(x)
                                                    x[rn,4]}))
rownames(Test) <- rn
L = apply( Test , 1 ,FUN = function(x){length(unique(x))})
if ( length(table(L)) != 1 | (all(Test[,1] == rn) == FALSE) ){
  stop("Exiting. Please check the guide RNA order between samples")
}

# transform into count matrix; the order is the same in all COUNTS list entries
crMat <- do.call(cbind, lapply(COUNTS, function(x){  x[rn,2]}))
# assign rownames
rownames(crMat) <- rn


# remove undetected guides
del = which(rowSums(crMat) == 0)
if (length(del) > 0){
  crMat = crMat[-del,]
}

print(paste0( length(del) ," guide RNAs have not been detected"))


# remove guides if barely detected in input samples
del = which(rowMins(crMat[,grep("nput", colnames(crMat))]) < 50)
if (length(del) > 0){
  crMat = crMat[-del,]
}

print(paste0( length(del) ," guide RNAs have been removed entirely due to low detection in input samples using a 50 read cutoff"))

# remove guides if not detected in any other sample
del = which(rowMins(crMat) == 0)
if (length(del) > 0){
  crMat = crMat[-del,]
}

print(paste0( length(del) ," guide RNAs have been removed entirely due to low detection in any samples using a 0 read cutoff"))

crMat.raw <- crMat

write.table(crMat.raw , file = "./data/CD71_count_matrix_raw.csv", sep=",", col.names = T, row.names = T, quote = F)






#### normalize counts

cat( paste0("\n Normalize counts: ",date(),"\n"))

# see https://hbctraining.github.io/DGE_workshop/lessons/02_DGE_count_normalization.html

mat=crMat.raw

# STEP 1 - build pseudo reference
PseudoRef <- apply(mat, MARGIN = 1, gm_mean)

# STEP 2 - normalize w/ median of ratios
Norm.mat.list <- NormalizeLikeDEseq( x= mat, ref = PseudoRef)

crMat.norm <- Norm.mat.list$normalized_counts

write.table(crMat.norm , file = "./data/CD71_count_matrix_normalized.csv", sep=",", col.names = T, row.names = T, quote = F)





#### Batch correction

cat( paste0("\n Batch correction: ",date(),"\n"))

# normalized count matrix
mat=crMat.norm

# get batch info
# batchMat <- cbind(colnames(mat), sapply(colnames(mat),function(x){strsplit(x, split="_")[[1]][1]}),sapply(colnames(mat),function(x){strsplit(x, split="_")[[1]][2]}))
# colnames(batchMat) <- c("sample","batch","bin")
batchMat <- cbind(colnames(mat), sapply(colnames(mat),function(x){strsplit(x, split="_")[[1]][1]}))
colnames(batchMat) <- c("sample","batch")


# Run sva embedded in BatchRemove from Mageckflute
RemovedBatch <- BatchRemove(mat = mat, batchMat = batchMat  , log2trans = T)

crMat.BatchCorrected.log <- RemovedBatch[[1]] 

crMat.BatchCorrected <- apply(crMat.BatchCorrected.log , MARGIN = 2, exp)

write.table(crMat.BatchCorrected , file = "./data/CD71_count_matrix_normalized_batchcorrected.csv", sep=",", col.names = T, row.names = T, quote = F)






#### Mask outlier

cat( paste0("\n Mask outliers: ",date(),"\n"))


#assign mat
mat = as.data.frame(crMat.BatchCorrected.log)
mat.count.orig = as.data.frame(crMat.BatchCorrected)

# initiate outlier quantiles
q=c(0.98,0.985,0.99,0.995) # for this work we chose 0.98 and used the residuals as opposed to Cooks or Mahalanobis

# initiate output lists
COOKS.cutoffMat.list <- list()
RES.cutoffMat.list <- list()
MAHALANOBIS.cutoffMat.list <- list()
COOKS.masked.list <- list()
RES.masked.list <- list()
MAHALANOBIS.masked.list <- list()


cat("\nThe error message concerns the 'self comparison' in calculating the Mahalanobis distance and can be ignored\n\n")
for (j in 1:length(q)){

  QUANT=q[j]

  # initiate tmp lists
  COOKS <- list()
  RES <- list()
  MAHALANOBIS <- list()

  #get measures
  # do comparison within replicate bins
  BINS = c("input", "top","bot")
  
  for (k in 1:length(BINS)){

    cooks <- list()
    res <- list()
    mh <- list()
    
    ma = mat[,grep(BINS[k] , colnames(mat))]
    cnt = 0
    for (i in 1:ncol(ma)){
      for (ii in 1:ncol(ma)){
        if (i == ii){
          next
        }
        else if(i > ii){
          next
        }
        else{
          cnt = cnt + 1
          fit <- lm(ma[,i] ~ ma[,ii], data = ma)
          cooks[[cnt]]  <- cooks.distance(fit)
          res[[i]] <- abs(fit$residuals)
          # The error message concerns the 'self comparison' in calculating the Mahalanobis distance and can be ignored
          mh[[cnt]] <- try(mahalanobis(ma[,c(i,ii)], colMeans(ma[,c(i,ii)]), cov(ma[,c(i,ii)])))
        }
      }
    }

    name <- BINS[k]

    res.mat <- do.call(cbind, res)
    RES[[k]] <- rowMedians(res.mat)
    names(RES)[k] <- name

    cooks.mat <- do.call(cbind, cooks)
    COOKS[[k]] <- rowMedians(cooks.mat)
    names(COOKS)[k] <- name

    mh.mat <- do.call(cbind, mh)
    MAHALANOBIS[[k]] <- rowMedians(mh.mat)
    names(MAHALANOBIS)[k] <- name

  }

  # tranform to matrix
  RES.mat <- (do.call(cbind, RES))
  COOKS.mat <- (do.call(cbind, COOKS))
  MH.mat <- (do.call(cbind, MAHALANOBIS))
  


  # get global cutoffs
  Q <- quantile(as.matrix(RES.mat), probs = QUANT)
  C <- quantile(as.matrix(COOKS.mat), probs = QUANT)
  M <- quantile(as.matrix(MH.mat), probs = QUANT)
  TC <- quantile(as.matrix(mat), probs = 0.66)

  # apply global cutoff
  for (i in 1:ncol(RES.mat)){
    RES.mat[,i] <- ifelse(RES.mat[,i] > Q , TRUE, FALSE)
    COOKS.mat[,i] <- ifelse( COOKS.mat[,i] > C , TRUE, FALSE)
    MH.mat[,i] <- ifelse( MH.mat[,i] > M, TRUE, FALSE)
  }
  
  #plot( mat.count.orig[, 3 ] , mat.count.orig[, 4 ])
  #plot( mat.count.orig[-which(COOKS.mat[,2] == T), 3 ] , mat.count.orig[-which(COOKS.mat[,2] == T), 4 ])
  

  # store intermediate data
  RES.cutoffMat.list[[j]] <- RES.mat
  COOKS.cutoffMat.list[[j]] <- COOKS.mat
  MAHALANOBIS.cutoffMat.list[[j]] <- MH.mat

  # name lists
  names(RES.cutoffMat.list)[j] <- paste0(c("res",QUANT), collapse = "_")
  names(COOKS.cutoffMat.list)[j] <- paste0(c("cooks",QUANT), collapse = "_")
  names(MAHALANOBIS.cutoffMat.list)[j] <- paste0(c("mh",QUANT), collapse = "_")

  # mask values
  mat.count = mat.count.orig
  for (k in 1:length(BINS)){
    mat.count[ which( RES.cutoffMat.list[[j]][, grep(BINS[k], colnames(RES.cutoffMat.list[[j]]))] == 1) ,grep(BINS[k], colnames(mat.count))] <- NA
  }
  RES.masked.list[[j]] <- mat.count
  
  mat.count = mat.count.orig
  for (k in 1:length(BINS)){
    mat.count[ which( COOKS.cutoffMat.list[[j]][, grep(BINS[k], colnames(COOKS.cutoffMat.list[[j]]))] == 1) ,grep(BINS[k], colnames(mat.count))] <- NA
  }
  COOKS.masked.list[[j]] <- mat.count
  
  mat.count = mat.count.orig
  for (k in 1:length(BINS)){
    mat.count[ which( MAHALANOBIS.cutoffMat.list[[j]][, grep(BINS[k], colnames(MAHALANOBIS.cutoffMat.list[[j]]))] == 1) ,grep(BINS[k], colnames(mat.count))] <- NA
  }
  MAHALANOBIS.masked.list[[j]] <- mat.count
  
  

  # name lists
  names(RES.masked.list)[j] <- paste0(c("res",QUANT), collapse = "_")
  names(COOKS.masked.list)[j] <- paste0(c("cooks",QUANT), collapse = "_")
  names(MAHALANOBIS.masked.list)[j] <- paste0(c("mh",QUANT), collapse = "_")

}

Masked.list <- c(list(crMat.BatchCorrected),RES.masked.list,COOKS.masked.list,MAHALANOBIS.masked.list)
names(Masked.list)[1] <- "NormBatchCorrected"

crMat.processedCounts = Masked.list[["res_0.995"]] # for this work we chose 0.995 and used the residuals
# remove guides entirely if it has been masked in any of the conditions
del = which( rowSums(t(apply( crMat.processedCounts , MARGIN = 1, FUN = function(x){is.na(x)}) )) > 0)
if (length(del) > 0){
  crMat.processedCounts = crMat.processedCounts[-del,]
}

print(paste0( length(del) ," guide RNAs have been removed entirely due to outlier detection using a 0.05% residual cutoff"))

write.table(crMat.processedCounts , file = "./data/CD71_count_matrix_processed.csv", sep=",", col.names = T, row.names = T, quote = F)




#### plot examples for outlier masking

cat( paste0("\n Plot outlier maksing example: ",date(),"\n"))


cols<-c("R1_input", "R2_input")
mat = as.data.frame(crMat.BatchCorrected.log)

df <- mat[,cols]
df$res <- apply( as.matrix(Masked.list[["res_0.995"]][,cols]) , MARGIN = 1, FUN = function(x){ifelse(is.na(max(x)) , TRUE, FALSE)})
df$cooks <- apply( as.matrix(Masked.list[["res_0.995"]][,cols]) , MARGIN = 1, FUN = function(x){ifelse(is.na(max(x)) , TRUE, FALSE)})
df$mh <- apply( as.matrix(Masked.list[["res_0.995"]][,cols]) , MARGIN = 1, FUN = function(x){ifelse(is.na(max(x)) , TRUE, FALSE)})

df$res <- factor(df$res)
df$cooks <- factor(df$cooks)
df$mh <- factor(df$mh)

g = list()

g[[1]] <- ggplot(aes(x=df[,1], y=df[,2]), data = df) +
      geom_point(aes(color = res), alpha = 0.2) +
      scale_color_manual(values=c("#999999", "#de2d26")) +
      xlab(paste0(cols[1] , " log2(counts)")) +
      ylab(paste0(cols[2] , " log2(counts)")) +
      theme_classic() +
      labs(colour="masked outlier") +
      ggtitle("Residuals") +
      ylim(c(2,10)) + xlim(c(2,10)) + coord_fixed(ratio = 1)
g[[2]] <- ggplot(aes(x=df[,1], y=df[,2]), data = df) +
  geom_point(aes(color = cooks), alpha = 0.2) +
  scale_color_manual(values=c("#999999", "#de2d26")) +
  xlab(paste0(cols[1] , " log2(counts)")) +
  ylab(paste0(cols[2] , " log2(counts)")) +
  theme_classic() +
  labs(colour="masked outlier") +
  ggtitle("Cooks Distance") +
  ylim(c(2,10)) + xlim(c(2,10)) + coord_fixed(ratio = 1)
g[[3]] <- ggplot(aes(x=df[,1], y=df[,2]), data = df) +
  geom_point(aes(color = mh), alpha = 0.2) +
  scale_color_manual(values=c("#999999", "#de2d26")) +
  xlab(paste0(cols[1] , " log2(counts)")) +
  ylab(paste0(cols[2] , " log2(counts)")) +
  theme_classic() +
  labs(colour="masked outlier") +
  ggtitle("Mahalanobis Distance") +
  ylim(c(2,10)) + xlim(c(2,10)) + coord_fixed(ratio = 1)


cols<-c("R1_top", "R2_top")
mat = as.data.frame(crMat.BatchCorrected.log)

DF <- mat[,cols]
DF$res <- apply( as.matrix(Masked.list[["res_0.995"]][,cols]) , MARGIN = 1, FUN = function(x){ifelse(is.na(max(x)) , TRUE, FALSE)})
DF$cooks <- apply( as.matrix(Masked.list[["cooks_0.995"]][,cols]) , MARGIN = 1, FUN = function(x){ifelse(is.na(max(x)) , TRUE, FALSE)})
DF$mh <- apply( as.matrix(Masked.list[["mh_0.995"]][,cols]) , MARGIN = 1, FUN = function(x){ifelse(is.na(max(x)) , TRUE, FALSE)})

DF$res <- factor(DF$res)
DF$cooks <- factor(DF$cooks)
DF$mh <- factor(DF$mh)


g[[4]] <- ggplot(aes(x=DF[,1], y=DF[,2]), data = DF) +
  geom_point(aes(color = res), alpha = 0.2) +
  scale_color_manual(values=c("#999999", "#de2d26")) +
  xlab(paste0(cols[1] , " log2(counts)")) +
  ylab(paste0(cols[2] , " log2(counts)")) +
  theme_classic() +
  labs(colour="masked outlier") +
  ggtitle("Residuals") +
  ylim(c(2,10)) + xlim(c(2,10)) + coord_fixed(ratio = 1)
g[[5]] <- ggplot(aes(x=DF[,1], y=DF[,2]), data = DF) +
  geom_point(aes(color = cooks), alpha = 0.2) +
  scale_color_manual(values=c("#999999", "#de2d26")) +
  xlab(paste0(cols[1] , " log2(counts)")) +
  ylab(paste0(cols[2] , " log2(counts)")) +
  theme_classic() +
  labs(colour="masked outlier") +
  ggtitle("Cooks Distance") +
  ylim(c(2,10)) + xlim(c(2,10)) + coord_fixed(ratio = 1)
g[[6]] <- ggplot(aes(x=DF[,1], y=DF[,2]), data = DF) +
  geom_point(aes(color = mh), alpha = 0.2) +
  scale_color_manual(values=c("#999999", "#de2d26")) +
  xlab(paste0(cols[1] , " log2(counts)")) +
  ylab(paste0(cols[2] , " log2(counts)")) +
  theme_classic() +
  labs(colour="masked outlier") +
  ggtitle("Mahalanobis Distance") +
  ylim(c(2,10)) + xlim(c(2,10)) + coord_fixed(ratio = 1)

pdf("./figures/QC_CD71_outlier_masking.pdf", width = 12, height = 8, useDingbats = F)
grid.arrange(g[[1]],g[[2]],g[[3]],g[[4]],g[[5]],g[[6]],nrow=2)
dev.off()






####### Visualize data structure improvement due to processing

cat( paste0("\n Plot data structure improvement due to processing: ",date(),"\n"))

# define symmetric breaks and generalizing color scheme
co = c(cor(crMat.raw, method = "spearman"), cor(crMat.norm, method = "spearman"),  cor(crMat.BatchCorrected, method = "spearman"), cor(crMat.processedCounts, method = "spearman"))
mn = co[which(co == min(co, na.rm = T))[1]] 
mx = co[which(co == max(co, na.rm = T))[1]] 
s <- mx - mn
breaksList.sp = seq(mn,mx, by = s/100)

co = c(cor(log2(crMat.raw), method = "pearson"), cor(log2(crMat.norm), method = "pearson"),  cor(log2(crMat.BatchCorrected), method = "pearson"),  cor(log2(crMat.processedCounts), method = "pearson"))
mn = co[which(co == min(co, na.rm = T))[1]] 
mx = co[which(co == max(co, na.rm = T))[1]] 
s <- mx - mn
breaksList.pe = seq(mn,mx, by = s/100)

annotation = data.frame(Bin = factor(rep(c(1:3),each = 2), labels = c("Input", "Top", "Bottom")),
                        Replicate = factor(rep(c(1:2),times = 3), labels = c("1", "2")))
Bin = (c("#f0f0f0", "#3182bd", "#de2d26"))
names(Bin) = c("Input", "Top", "Bottom")
Replicate = c("#7a0177", "#fa9fb5")
names(Replicate) = c("1", "2")
ann_colors = list(Bin = Bin, Replicate = Replicate)



pdf("./figures/QC_CD71_processing.pdf", height = 12, width = 10, useDingbats = F, onefile=FALSE)
par(mfcol=c(4,2))
suppressMessages(aheatmap( cor(crMat.raw, method = "spearman") , distfun = "euclidean", hclustfun = "ward", cellwidth = 10, cellheight = 10,  breaks = breaksList.sp, annCol = annotation, annRow = annotation,  annColors = ann_colors, labCol = NA, sub = "raw counts"))
text(x = -3.5 , y = 4 , "spearman coef.", srt = 90 )
suppressMessages(aheatmap(cor(crMat.norm, method = "spearman"), distfun = "euclidean", hclustfun = "ward", cellwidth = 10, cellheight = 10,  breaks = breaksList.sp, annCol = annotation, annRow = annotation,  annColors = ann_colors, labCol = NA, sub = "normalized counts"))
text(x = -3.5, y = 4 , "spearman coef.", srt = 90)
suppressMessages(aheatmap(cor(crMat.BatchCorrected, method = "spearman"), distfun = "euclidean", hclustfun = "ward", cellwidth = 10, cellheight = 10,  breaks = breaksList.sp, annCol = annotation, annRow = annotation,  annColors = ann_colors, labCol = NA, sub = "batch-corrected normalized counts"))
text(x = -3 , y = 4 , "spearman coef.", srt = 90)
suppressMessages(aheatmap(cor(crMat.processedCounts, method = "spearman", use="complete.obs"), distfun = "euclidean", hclustfun = "ward", cellwidth = 10, cellheight = 10,  breaks = breaksList.sp, annCol = annotation, annRow = annotation,  annColors = ann_colors, labCol = NA,  sub = "batch-corrected normalized outlier-removed counts"))
text(x = -3 , y = 4 , "spearman coef.", srt = 90)

suppressMessages(aheatmap(cor(log2(crMat.raw), method = "pearson"), distfun = "euclidean", hclustfun = "ward",cellwidth = 10, cellheight = 10,  breaks = breaksList.pe, annCol = annotation, annRow = annotation,  annColors = ann_colors, labCol = NA, sub = "raw counts"))
text(x = -2.4 , y = 4 , "pearson coef.", srt = 90)
suppressMessages(aheatmap(cor(log2(crMat.norm), method = "pearson"), distfun = "euclidean", hclustfun = "ward", cellwidth = 10, cellheight = 10,  breaks = breaksList.pe, annCol = annotation, annRow = annotation,  annColors = ann_colors, labCol = NA, sub = "normalized counts"))
text(x = -2.4 , y =4 , "pearson coef.", srt = 90)
suppressMessages(aheatmap(cor(log2(crMat.BatchCorrected), method = "pearson"), distfun = "euclidean", hclustfun = "ward", cellwidth = 10, cellheight = 10,  breaks = breaksList.pe, annCol = annotation, annRow = annotation,  annColors = ann_colors, labCol = NA, sub = "batch-corrected normalized counts"))
text(x = -2  , y = 4 , "pearson coef.", srt = 90)
suppressMessages(aheatmap(cor(log2(crMat.processedCounts), method = "pearson", use = "complete.obs"), distfun = "euclidean", hclustfun = "ward", cellwidth = 10, cellheight = 10,  breaks = breaksList.pe, annCol = annotation, annRow = annotation,  annColors = ann_colors, labCol = NA, sub = "batch-corrected normalized outlier-removed counts"))
text(x = -2  , y = 4 , "pearson coef.", srt = 90)
dev.off()




# PCA
Transformations <- list(crMat.raw, crMat.norm, crMat.BatchCorrected, crMat.processedCounts)
names(Transformations) <- c("raw","norm","batch-corrected","outlier-removed")
P = list()

for ( i in 1:length(Transformations)){
  P[[i]] = Plot.pca(Transformations[[i]], NAME = names(Transformations)[i])
}

pdf("./figures/QC_CD71_PCA.pdf", width = 8, height = 16, useDingbats = F)
grid.arrange(P[[1]][[1]],P[[1]][[2]],P[[2]][[1]],P[[2]][[2]],P[[3]][[1]],P[[3]][[2]],P[[4]][[1]],P[[4]][[2]], ncol=2)
dev.off()











####### Assign CRISPR SCORE (= log2(Bin read count / Input read count))

cat( paste0("\nCalculate CRISPR Scores: ",date(),"\n"))

#calculate CRISPR SCORE (CS) == log2(Bin/Input)
Masked.list = list((crMat.raw),(crMat.norm),(crMat.BatchCorrected),(crMat.processedCounts))
names(Masked.list) = c("raw","norm","batch_corrected","outlier-removed")
CS.list <- lapply( Masked.list , AssignCScore )



############################################

# Correlate CS
i = grep("outlier-removed" ,names(CS.list))
CS = CS.list[[i]]

annotation = data.frame(Bin = factor(rep(c(1:2),each = 2), labels = c( "Top", "Bottom")),
                        Replicate = factor(rep(c(1:2),times = 2), labels = c("1", "2")))
Bin = (c("#3182bd", "#de2d26"))
names(Bin) = c("Top", "Bottom")
Replicate = c("#7a0177", "#fa9fb5")
names(Replicate) = c("1", "2")
ann_colors = list(Bin = Bin, Replicate = Replicate)


ma = cor(CS, method = "pearson", use = "complete.obs")
suppressMessages(aheatmap(ma, distfun = "euclidean", hclustfun = "ward", cellwidth = 12, cellheight = 12,  annCol = annotation, annRow = annotation,  annColors = ann_colors, labCol = NA, main = "guide enrichment correlation", sub = "norm. counts - pearson coef.",
         filename = "./figures/QC_CD71_CRISPRscores_Pearson_Correlation.pdf"))




###### Run Robust Rank Aggregation (RRA)

cat( paste0("\nRun Robust Rank Aggregation (RRA): ",date(),"\n"))

g <- list()
for (i in 1:length(CS.list)){
  g[[i]] <- RunRRA(CS.list[[i]], NAME = names(CS.list)[i])
}
names(g) = names(CS.list)


###### Plot RRA results
i = grep("outlier-removed" ,names(CS.list))

pdf("./figures/QC_CD71_CRISPRscore_vs_RRA.pdf", width = 10, height = 4, useDingbats = F)
grid.arrange(g[[i]][[2]] + xlim(c(-1.75,1.75)) + ylim(c(0,7)) + coord_fixed( r = (2*1.75)/7 ),
                   g[[i]][[3]] + xlim(c(-1.75,1.75)) + ylim(c(0,7)) + coord_fixed( r = (2*1.75)/7 ),ncol=2, nrow = 1)
dev.off()

pdf("./figures/FigS9d_QC_CD71_CRISPRscore_Violin.pdf", width = 10, height = 4, useDingbats = F)
grid.arrange(g[[i]][[4]] + ylim(c(-1.75,1.75)) + coord_fixed( r = 7/(2*1.75) ),
                   g[[i]][[5]] + ylim(c(-1.75,1.75)) + coord_fixed( r = 7/(2*1.75) ),ncol=2, nrow = 1)
dev.off()




CD71_Results = g[[i]][[1]]

write.table( CD71_Results , file = "./data/CD71_screen_crRNA_enrichments.csv", col.names = T, row.names = T,quote = F, sep = ",")

#breaksList = seq(-1,1, by = 0.02)
ma = cor(CD71_Results[,grep("meanCS",colnames(CD71_Results))], method = "pearson", use = "complete.obs")
suppressMessages(aheatmap(ma, distfun = "euclidean", hclustfun = "ward", cellwidth = 25, cellheight = 25, 
         labRow = gsub("meanCS.","",rownames(ma)), labCol = gsub("meanCS.","",colnames(ma)), fontsize = 5, main = "guide RNA enrichment", sub = "norm. counts - pearson coef.",
         txt = round(ma,2), filename = "./figures/QC_CD71_screen_crRNA_enrichments_PearsonCorrelation.pdf"))


RRA.CD71.result.list <- g
save(RRA.CD71.result.list, file = './data/RRA.CD71.result.list.RData')





