#!/bin/bash
#$ -cwd

SCRIPTS="./scripts/"
FILES="./data/"
V="RFgfp" # allowed values v1 to v10 and RFgfp; v3 showed the best performance v3==final
N=1000 # N=1000




# Run boot strapping only if output file doesn't exist already
if [[ ! -f "${FILES}Learning_approaches_Rsqured_${V}.csv" || ! -f "${FILES}Learning_approaches_Spearman_${V}.csv" || ! -f "${FILES}Features_${V}.RData" ]]; then 
    

	PerfectMatches="${FILES}CompleteFeatureSet.csv" 

	for i in $(eval echo "{1..$N}");do

     	echo -e "Rscript ${SCRIPTS}prepare_datasets.R ${PerfectMatches} $i normCS ${V}" | qsub -V -cwd -j y -N job${V}${i} -e /dev/null -o /dev/null 
	
	done 




	# wait for completion of loop above by waiting for the last file to be created
	echo -e "\n\t ...waiting for jobs to complete\n\n"
	while [[ ! -f ./tmp_RData_${V}/ModelData-${N}.RData ]];
	do
	    sleep 1
	done;
	
	echo -e "\n\t ...jobs completed!\n\n"





	# Evaluate learning procedures doing bootstrapping.
	# 1000x 70/30 splits of train and test data
	# replace 990 with 1 for full set
	for i in $(eval echo "{1..$N}");do
	
		# here I write stdout and stderr to /dev/null
		# incase you do not get the expected Model.rho.txt/Model.sp.txt/FeatureSelection.txt it may be because some R packages are missing and need to be installed.
		# In that case, you may want to keep stdout and stderr
		# each job may take ~30min because of SVM tuning.
	    echo -e "Rscript ${SCRIPTS}/regression_models_Cas13.R $i ${V} ${FILES}formulas.txt" | qsub -V -cwd -j y -N job${V}${i} # -e /dev/null -o /dev/null 

	done 





	# wait for completion of loop above by waiting for the last file to be created. 
	# It may take a little longer per job, and #1000 may finish before #99x, thus the loop is followed by an addional 2min wait 
	echo -e "\n\t ...waiting for jobs to complete.
			If this takes > 30min, please check if regression_models.R produced files.txt output.
			If no output was generated, you are likely missing some R packages.\n\n"
	while [[ ! -f ./tmp_ModelData_${V}/Model.rho_${N}.txt ]];
	do
	    sleep 1
	done;
	
	echo -e "\n\t ...jobs completed!\n\n"





	# Plot learning procedure performance
	Rscript ${SCRIPTS}EvaluateModels.R ${V}  # Generates FigS10c






	# remove intermediate files once all plot have been generated
	# while [ ! -f Model_Features_Lasso_StepBIC_${V}.pdf ];
	#do
	# sleep 1
	#done;
  #rm ./tmp_RData/ModelData*.RData ./tmp_ModelData/Model.rho_*.txt ./tmp_ModelData/Model.spearman_*.txt ./tmp_ModelData/FeatureSelection_*.txt

else


	# Plot learning procedure performance
	Rscript ${SCRIPTS}EvaluateModels.R ${V} # Generates FigS10c


fi