#!/usr/bin/env Rscript

###### set variables; load libs #####################
.libPaths(new="~/R/x86_64-redhat-linux-gnu-library/3.5")
suppressMessages(library(randomForest, quietly = T))
suppressMessages(library(caret, quietly = T))
suppressMessages(library(ggplot2, quietly = T))
suppressMessages(library(reshape2, quietly = T))
suppressMessages(library(cowsay, quietly = T))


###### sub-routines #################################

########################## sub-routines end  ########

###### execute ######################################


args = commandArgs(trailingOnly=TRUE)
if (length(args)!=4) {
  stop("Exiting! Please run < Rscript ./scripts/Generate_final_RandomForestModel.R ./data/CompleteFeatureSet.csv normCS final ./data/formulas.txt >", call.=FALSE)
} else if (length(args)==4) {
  fullset = read.delim(args[1], header = T, sep = ",", stringsAsFactors = F)  # fullset = read.delim("./data/CompleteFeatureSet.csv", header = T, sep = ",", stringsAsFactors = F)
  VALUE = as.character(args[2]) #  VALUE = "normCS"
  Version = as.character(args[3]) # Version = "final"
  Formulas = as.character(args[4]) #  Formulas = "./data/formulas.txt"
}


########## Execution

# get relevant fields
# formula to test along with respective fields
FORMULAS = read.delim( file = Formulas , header = F , sep = "\t" , stringsAsFactors = F )
idx = grep(Version , FORMULAS$V1)
feats = gsub(" ","",gsub("\n","",strsplit( x = FORMULAS$V3[idx] , split = ",")[[1]]))
fields = c( "Screen" , FORMULAS$V2[idx] , feats)
grouped.formula = as.formula( paste0(c(fields[2] , "~" , paste0(fields[-c(1:2)] , collapse = " + ")), collapse = " "))

# which response value to use
colnames(fullset)[which(colnames(fullset) == VALUE)] = 'value'


# restrict to complete cases
fullset = fullset[complete.cases(fullset[,fields]),fields]

# find linear dependencies
# relative to Model v3, these features have been removed : c('NTdens_min_GC','pT','pG.pC','pA.pT','pTT')
S = fullset[,grep("Screen|value",colnames(fullset)) ]
fullset = fullset[,grep("Screen|value",colnames(fullset), invert = T) ]
dependencies = findLinearCombos(fullset) #uses QR decomposition to find linear dependencies in matrix (caret package)
if(length(dependencies$remove) > 0){
  
  del = colnames(fullset)[dependencies$remove]
  
  fullset = fullset[, -c( which( colnames(fullset) %in% del))]
  
  rm = which( fields %in% del)
  if(length(rm) > 0){
    fields = fields[-rm]
  }
  
  f = as.character(grouped.formula)
  tmp = gsub(" ","",gsub("\n","",strsplit(f[3], split = "\\+")[[1]]))
  rm = which( tmp %in% del)
  if(length(rm) > 0){
    tmp = tmp[-rm]
  }
  f[3] = paste0(tmp , collapse = "+")
  grouped.formula = as.formula( paste0(c(f[2] , f[1] , f[3]), collapse = " "))
  
}

fullset = cbind.data.frame(S,fullset)




# scale data
numeric = sapply(fullset, is.numeric) & colnames(fullset) != "value"  & colnames(fullset) != "Gquad"  & colnames(fullset) != "DR" & colnames(fullset) != "Screen" & grepl("[A,C,G,T]_",colnames(fullset)) == FALSE
if (sum(numeric) > 0){
  tmpmean = apply(fullset[,numeric], 2, function(x) quantile(x, 0.05))
  tmpsd = apply(fullset[,numeric], 2, function(x) quantile(x, 0.95)-quantile(x, 0.05))
  fullset[,numeric] = scale(fullset[,numeric], center=tmpmean, scale=tmpsd)
  fullset[,numeric][fullset[,numeric] > 1] = 1
  fullset[,numeric][fullset[,numeric] < 0] = 0
}


FULLSET = fullset[,grep("Screen", colnames(fullset),invert = T)]


# Tuning did not significantly improve model performace
# Random Forest Tuning
customRF <- list(type = "Regression", library = "randomForest", loop = NULL)
customRF$parameters <- data.frame(parameter = c("mtry", "ntree"), class = rep("numeric", 2), label = c("mtry", "ntree"))
customRF$grid <- function(x, y, len = NULL, search = "grid") {}
customRF$fit <- function(x, y, wts, param, lev, last, weights, classProbs, ...) {
  randomForest(x, y, mtry = param$mtry, ntree=param$ntree, ...)
}
customRF$predict <- function(modelFit, newdata, preProc = NULL, submodels = NULL)
  predict(modelFit, newdata)
customRF$prob <- function(modelFit, newdata, preProc = NULL, submodels = NULL)
  predict(modelFit, newdata, type = "prob")

seed <- 333
metric <- "RMSE"
control <- trainControl(method="repeatedcv", number=10, repeats=3)
tunegrid <- expand.grid(.mtry=c(1:14), .ntree=seq(250,4000,250))
set.seed(seed)
custom <- train(value~. , data=FULLSET, method=customRF, metric=metric, tuneGrid=tunegrid, trControl=control)

saveRDS( object = custom , file = './data/RandomForestTune.RDS')

TunedRF = readRDS(file = './data/RandomForestTune.RDS')


cat('\nBest Random Forest tuning result with:\n')
cat('\tmtry = 12\n')
cat('\ttree = 2000\n')

pdf('./figures/RandomForestTuning.pdf' , width = 8, height = 4, useDingbats = F)
plot(TunedRF)
dev.off()



# Train model on full data set
set.seed(seed)
RandomForestModel = randomForest(fullset[,-c(1:2)], fullset[,2], importance = T , ntree = 2000 , mtry = 12 ) # from randomForest package

txt = importance(RandomForestModel)
write.table(txt , file = paste0("./data/RandomForestModel_FeatureImportance_",VALUE,"_",Version,".txt") ,quote = F , sep = '\t')

pdf('./figures/RandomForestTuningFeatureImportance.pdf' , width = 8, height = 6, useDingbats = F)
par(mfrow=c(1,2))
varImpPlot(RandomForestModel,type=1 ,n.var = 35 )
varImpPlot(RandomForestModel,type=2 ,n.var = 35 )
dev.off()

save(RandomForestModel, file = "RandomForestModel.RData")











# ############ Plot Decision tree # https://shiring.github.io/machine_learning/2017/03/16/rf_plot_ggraph
# 
# library(dplyr)
# library(ggraph)
# library(igraph)
# 
# df = data.frame(c(vector of feature names as in input df),
#                  c(vector of feature labels), stringsAsFactors = F)
# colnames(df) = c("long","short")
# 
# 
# tree_func <- function(final_model, tree_num, var_table = df) {
#   
#   # get tree by index
#   tree <- randomForest::getTree(final_model, 
#                                 k = tree_num, 
#                                 labelVar = TRUE) %>%
#     tibble::rownames_to_column() # %>%
#     # make leaf split points to NA, so the 0s won't get plotted
#     # % mutate(`split point` = ifelse(is.na(prediction), `split point`, NA))
#   
#   tree$`split var` = as.character(tree$`split var`)
#   
#   for( i in 1:nrow(df)){
#     idx = which(as.character(tree$`split var`) == df$long[i])
#     tree$`split var`[idx] = df$short[i]
#   }
#   
#   tree$`split var` = as.factor(tree$`split var`)
#   
#   
#   # prepare data frame for graph
#   graph_frame <- data.frame(from = rep(tree$rowname, 2),
#                             to = c(tree$`left daughter`, tree$`right daughter`))
#   
#   # convert to graph and delete the last node that we don't want to plot
#   graph <- graph_from_data_frame(graph_frame) %>%
#     delete_vertices("0")
#   
#   # set node labels
#   V(graph)$node_label <- gsub("_", " ", as.character(tree$`split var`))
#   V(graph)$leaf_label <- as.character(round(tree$prediction,2))
#   V(graph)$split <- as.character(round(tree$`split point`, digits = 2))
#   V(graph)$split <- ifelse(V(graph)$split != "0", V(graph)$split , NA)
#   
#   # plot
#   plot <- ggraph(graph, 'dendrogram') + 
#     theme_bw() +
#     geom_edge_link() +
#     geom_node_point() +
#     geom_node_text(aes(label = node_label), na.rm = TRUE, repel = TRUE, colour = "#070707", size = 8) +
#     geom_node_label(aes(label = split), vjust = 2.5, na.rm = TRUE, fill = "#fcfcfc", colour = "#252525") +
#     geom_node_label(aes(label = leaf_label, fill = leaf_label), na.rm = TRUE, 
#                     repel = TRUE, colour = "white", fontface = "bold", show.legend = FALSE) +
#     theme(panel.grid.minor = element_blank(),
#           panel.grid.major = element_blank(),
#           panel.background = element_blank(),
#           plot.background = element_rect(fill = "white"),
#           panel.border = element_blank(),
#           axis.line = element_blank(),
#           axis.text.x = element_blank(),
#           axis.text.y = element_blank(),
#           axis.ticks = element_blank(),
#           axis.title.x = element_blank(),
#           axis.title.y = element_blank(),
#           plot.title = element_text(size = 18))
#   
#   print(plot)
# }
# 
# 
# 
# model_rf = RandomForestModel
# tree_num <- which(RandomForestModel$forest$ndbigtree == min(RandomForestModel$forest$ndbigtree))
# 
# pdf("./figures/Tree.pdf", width = 30, height = 20, useDingbats = F)
# tree_func(final_model = model_rf, tree_num)
# dev.off()


