#!/bin/bash
#$ -cwd

SCRIPTS="./scripts/"
FILES="./data/"





# Run boot strapping only if output file doesn't exist already
if [[ ! -f "Learning_approaches_Rsqured_v1.csv" || ! -f "Learning_approaches_Spearman_v1.csv" || ! -f "Features_v1.RData" ]]; then 
    

	PerfectMatches="${FILES}GFPscreen_results_PerfectMatch.csv" # for all Model versions v1/v2

	# replace 990 with 1 for full set
	for i in {1..1000}; do 

     	echo -e "Rscript ${SCRIPTS}prepare_datasets.R ${PerfectMatches} $i" | qsub -V -cwd -j y -N job${i} -e /dev/null -o /dev/null 
	done 




	# wait for completion of loop above by waiting for the last file to be created
	echo -e "\n\t ...waiting for jobs to complete\n\n"
	while [[ ! -f ModelData-1000.RData ]];
	do
	    sleep 1
	done;
	sleep 1m
	echo -e "\n\t ...jobs completed!\n\n"





	# Evaluate learning procedures doing bootstrapping.
	# 1000x 70/30 splits of train and test data
	# replace 990 with 1 for full set
	for i in {1..1000}; do 
	
		# here I write stdout and stderr to /dev/null
		# incase you do not get the expected Model.rho.txt/Model.sp.txt/FeatureSelection.txt it may be because some R packages are missing and need to be installed.
		# In that case, you may want to keep stdout and stderr
		# each job may take ~30min because of SVM tuning.
	    echo -e "Rscript ${SCRIPTS}/regression_models_Cas13_v1.R $i" | qsub -V -cwd -j y -N job${i} -l mem=64G -e /dev/null -o /dev/null 

	done 





	# wait for completion of loop above by waiting for the last file to be created. 
	# It may take a little longer per job, and #1000 may finish before #99x, thus the loop is followed by an addional 2min wait 
	echo -e "\n\t ...waiting for jobs to complete.
			If this takes > 30min, please check if regression_models.R produced files.txt output.
			If no output was generated, you are likely missing some R packages.\n\n"
	while [[ ! -f Model.rho_1000.txt ]];
	do
	    sleep 1
	done;
	sleep 2m
	echo -e "\n\t ...jobs completed!\n\n"





	# Plot learning procedure performance
	Rscript ${SCRIPTS}EvaluateModels_v1.R






	# remove intermediate files once all plot have been generated
	while [ ! -f Model_Features_Lasso_StepBIC_v1.pdf ];
	do
	  sleep 1
	done;
  rm ModelData*.RData Model.rho_*.txt Model.spearman_*.txt FeatureSelection_*.txt

else


	# Plot learning procedure performance
	Rscript ${SCRIPTS}EvaluateModels_v1.R


fi
























# Run boot strapping only if output file doesn't exist already
if [[ ! -f "Learning_approaches_Rsqured_v2.csv" || ! -f "Learning_approaches_Spearman_v2.csv" || ! -f "Features_v2.RData" ]]; then 
    

	PerfectMatches="${FILES}GFPscreen_results_PerfectMatch.csv" # for all Model versions v1/v2

	# replace 990 with 1 for full set
	for i in {1..1000}; do 

     	echo -e "Rscript ${SCRIPTS}prepare_datasets.R ${PerfectMatches} $i" | qsub -V -cwd -j y -N job${i} -e /dev/null -o /dev/null 

	done 




	# wait for completion of loop above by waiting for the last file to be created
	echo -e "\n\t ...waiting for jobs to complete\n\n"
	while [[ ! -f ModelData-1000.RData ]];
	do
	    sleep 1
	done;
	sleep 1m
	echo -e "\n\t ...jobs completed!\n\n"





	# Evaluate learning procedures doing bootstrapping.
	# 1000x 70/30 splits of train and test data
	# replace 990 with 1 for full set
	for i in {1..1000}; do 
	
		# here I write stdout and stderr to /dev/null
		# incase you do not get the expected Model.rho.txt/Model.sp.txt/FeatureSelection.txt it may be because some R packages are missing and need to be installed.
		# In that case, you may want to keep stdout and stderr
		# each job may take ~30min because of SVM tuning.
	    echo -e "Rscript ${SCRIPTS}/regression_models_Cas13_v2.R $i" | qsub -V -cwd -j y -N job${i} -l mem=64G -e /dev/null -o /dev/null 

	done 





	# wait for completion of loop above by waiting for the last file to be created. 
	# It may take a little longer per job, and #1000 may finish before #99x, thus the loop is followed by an addional 2min wait 
	echo -e "\n\t ...waiting for jobs to complete.
			If this takes > 30min, please check if regression_models.R produced files.txt output.
			If no output was generated, you are likely missing some R packages.\n\n"
	while [[ ! -f Model.rho_1000.txt ]];
	do
	    sleep 1
	done;
	sleep 2m
	echo -e "\n\t ...jobs completed!\n\n"





	# Plot learning procedure performance
	Rscript ${SCRIPTS}EvaluateModels_v2.R






	# remove intermediate files once all plot have been generated
	while [ ! -f Model_Features_Lasso_StepBIC_v2.pdf ];
	do
	    sleep 1
	done;

	rm ModelData*.RData Model.rho_*.txt Model.spearman_*.txt FeatureSelection_*.txt

else


	# Plot learning procedure performance
	Rscript ${SCRIPTS}EvaluateModels_v2.R


fi
















	



# Run boot strapping only if output file doesn't exist already
if [[ ! -f "Learning_approaches_Rsqured_v3.csv" || ! -f "Learning_approaches_Spearman_v3.csv" || ! -f "Features_v3.RData" ]]; then 
    

	PerfectMatches="${FILES}TargetDiNucleotidesModelInputTable_PM.csv" # for Doench 2014 regression model


	# replace 990 with 1 for full set
	for i in {990..1000}; do 

     	echo -e "Rscript ${SCRIPTS}prepare_datasets.R ${PerfectMatches} $i" | qsub -V -cwd -j y -N job${i} -e /dev/null -o /dev/null 

	done 




	# wait for completion of loop above by waiting for the last file to be created
	echo -e "\n\t ...waiting for jobs to complete\n\n"
	while [[ ! -f ModelData-1000.RData ]];
	do
	    sleep 1
	done;
	sleep 1m
	echo -e "\n\t ...jobs completed!\n\n"


	echo -e "Rscript ${SCRIPTS}/regression_models_Cas13_v3.R 1" | qsub -V -cwd -j y -N job1 -l mem=64G # -e /dev/null -o /dev/null


	# Evaluate learning procedures doing bootstrapping.
	# 1000x 70/30 splits of train and test data
	# replace 990 with 1 for full set
	for i in {990..1000}; do 
	
		# here I write stdout and stderr to /dev/null
		# incase you do not get the expected Model.rho.txt/Model.sp.txt/FeatureSelection.txt it may be because some R packages are missing and need to be installed.
		# In that case, you may want to keep stdout and stderr
		# each job may take ~30min because of SVM tuning.
	    echo -e "Rscript ${SCRIPTS}/regression_models_Cas13_v3.R $i" | qsub -V -cwd -j y -N job${i} -l mem=64G -e /dev/null -o /dev/null 

	done 





	# wait for completion of loop above by waiting for the last file to be created. 
	# It may take a little longer per job, and #1000 may finish before #99x, thus the loop is followed by an addional 2min wait 
	echo -e "\n\t ...waiting for jobs to complete.
			If this takes > 30min, please check if regression_models.R produced files.txt output.
			If no output was generated, you are likely missing some R packages.
			(~35 of the 1000x bootstraps will have no output as models could not be found\n\n"
	while [[ ! -f Model.rho_1000.txt ]];
	do
	    sleep 1
	done;
	sleep 30m
	echo -e "\n\t ...jobs completed!\n\n"





	# Plot learning procedure performance
	Rscript ${SCRIPTS}EvaluateModels_v3.R






	# remove intermediate files once all plot have been generated
	while [ ! -f Model_Features_Lasso_StepBIC_v3.pdf ];
	do
	    sleep 1
	done;

	rm ModelData*.RData Model.rho_*.txt Model.spearman_*.txt FeatureSelection_*.txt

else


	# Plot learning procedure performance
	Rscript ${SCRIPTS}EvaluateModels_v3.R


fi













exit



