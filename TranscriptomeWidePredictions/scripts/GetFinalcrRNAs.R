#!/usr/bin/env Rscript


###### load libs ##########################
.libPaths(new="~/R/x86_64-redhat-linux-gnu-library/3.5")
suppressMessages(library(dplyr, quietly = T))
suppressMessages(library(reshape2, quietly = T))
suppressMessages(library(ggplot2, quietly = T))
suppressMessages(library(gridExtra, quietly = T))
suppressMessages(library(grid, quietly = T))


###### load Reference #####################
Ref=read.delim("./data/gencode.v19.pc_transcripts.txt", sep = "|", stringsAsFactors = F, header = F)[1:10]
colnames(Ref) = c("ENST","ENSG","OTTHUMG","OTTHUMT","ID","Symbol","Length","UTR5","CDS","UTR3" ) # give colnames
Ref$ENST <- gsub(">","",Ref$ENST) # remove preceding greater symbol
# Adjust column content
Ref[which(grepl("UTR3",Ref[,9]) == T),10] <- Ref[which(grepl("UTR3",Ref[,9]) == T),9]
Ref[which(grepl("CDS",Ref[,8]) == T),9] <- Ref[which(grepl("CDS",Ref[,8]) == T),8]
Ref[which(grepl("CDS",Ref[,8]) == T),8] <- ""

GTF=read.delim("./data/gencode.v19.annotation.protein_coding.txt", sep = "\t", stringsAsFactors = F, header = F)
GTF=GTF[which(GTF$V3 == "transcript"),]

###### load input data ####################


# find files
cat( paste0( "fetching *.csv files started on " , date() , "\n"))
files = list.files( path = "./data",full.names = T, recursive = T, pattern = "*CasRxguides.csv")
if (length(files) == 0)(
  stop("Exiting! No files found. Please run bash SubmutArrayJobs.sh first!")
)

# testing
# files = files[1:100]

# load files
cat( paste0( "reading *.csv files started on " , date() , "\n"))
CSVs <- lapply(files , FUN = function(x){read.delim( x, sep = ',' , header = T , stringsAsFactors = F)})



###### sub-routines #######################

GetTxIDs=function(x){
  tmp=strsplit(x,split="; ")[[1]]
  return(gsub("transcript_id ","",tmp[grep("transcript_id",tmp)]))
}

GetName = function(x){
  gsub("_CasRxguides.csv","",basename(x))
}
GetCoord = function(x,y=2){
  as.numeric(strsplit(x, split = "-")[[1]][y])
}


AnnotateGuides = function(x = CSVs[[1]],ref = Ref[names(CSVs)[1],]){
  
  info = x
  
  u5 =  which( as.numeric(info$MatchPos) < as.numeric(ref$CDS.start))
  cds = which( as.numeric(info$MatchPos) >= as.numeric(ref$CDS.start) & (as.numeric(info$MatchPos)-nchar(info$GuideSeq)) <= as.numeric(ref$CDS.end))
  u3 =  which( (as.numeric(info$MatchPos) - nchar(info$GuideSeq)) > as.numeric(ref$CDS.end))  
  
  l = list(NULL,NULL,NULL,NULL)
  names(l) = c("UTR5","CDS","UTR3","All")
  if(length(u5) > 0){
    l[[1]] = info[u5,]
  }
  if(length(cds) > 0){
    l[[2]] = info[cds,]
  }
  if(length(u3) > 0){
    l[[3]] = info[u3,]
  }
  if(nrow(info) > 0){
    l[[4]] = info
  }
  return(l)
}



GetPredictionFraction = function(x = UTR5s[[1]]){
  if (is.null(x) == T){
    return(NA)
  }
  else{
    
    f = table(is.na(suppressWarnings(as.numeric(x$standardizedGuideScores))))/length(x$standardizedGuideScores)
    
    if (length(f) == 1){
      if(names(f) == "FALSE"){
        return(1)
      }
      else if(names(f) == "TRUE"){
        return(0)
      }
    }
    else if (length(f) == 2){
      return((table(is.na(suppressWarnings(as.numeric(x$standardizedGuideScores))))/length(x$standardizedGuideScores))["FALSE"])
    }
    else{
      stop("Exiting. Check function.")
    }
  }
}

GetPredictionQuartile = function(x = UTR5s[[3]]){
  if (is.null(x) == T ){
    return(NA)
  }
  else{
    
    if (length( which( is.na(x$quartile) == TRUE) ) > 0 ){
      x = x[which( is.na(x$quartile) == FALSE),]
      if(nrow(x) == 0){
        return(NA)
      }
      else{
        return(x$quartile)
      }
    }
    else{
      return(x$quartile)
    }
  }
}


concat=function(y){if(is.na(suppressWarnings(as.numeric(as.character(y[4])))) == T){return(NA)}else{paste0(y[1],"_",y[2],"_",round(as.numeric(as.character(y[5])),4) ,"_",round(as.numeric(as.character(y[6])),3))}}

GetTopGuides = function(x,n = 10, WHICH = "top", WHAT = "scores"){
  if(is.null(x)){
    if(WHAT == "scores"){
      return(NA)
    }else{
      return(rbind(rep(NA,n)))
    }
  }
  else if (is.na(n)){
    if(WHAT == "scores"){
      return(NA)
    }else{
      return(rbind(rep(NA,n)))
    }
  }
  else{
    
    if (WHICH == "top"){
      sel = x[1:min(n,nrow(x)),]
    }
    else if(WHICH == "random"){
      r = which( suppressWarnings(is.na( as.numeric(x$standardizedGuideScores))) == F)
      if (length(r) == 0){
        return(NA)
      }
      else{
        x = x[r,]
      }
      l=min(n,nrow(x))
      s = sort(sample(c(1:nrow(x)), size = l ,replace = F ))
      sel = x[s,]
    }else{
      stop("Exiting! WHICH needs to be \"top\" or \"random\".")
    }
    
    
    
    
    if(WHAT == "scores"){
      return(na.omit(suppressWarnings(as.numeric(as.character(sel$standardizedGuideScores)))))
    }
    else if(WHAT == "guides"){
      
      out = apply(sel,1,concat)
      
      if(length(out) < n){
        return(rbind(c(out,rep(NA,n-length(out)))))
      }else{
        return(rbind(out))
      }
    }else{
      stop("Exiting! WHAT needs to be \"scores\" or \"guides\".")
    }
  }
}

GetTopBot = function(x = GuideAnnotaion[[1]], TOP = 0.75, BOTTOM = 0.25, NAME = "All"){
  if (is.null(x) == T){
    return(NA)
  }
  else{
    
    x = x[[NAME]]
    
    
    if (length( which( is.na(x$quartile) == TRUE ) ) > 0   ) {
      x = x[which( is.na(x$quartile) == FALSE ),]
      if(nrow(x) == 0){
        return(NA)
      }
      else{
        Q = quantile(as.numeric(x$standardizedGuideScores) , probs = c(BOTTOM,TOP))
        bot = x[which(as.numeric(x$standardizedGuideScores) < Q[1]),]
        top = x[which(as.numeric(x$standardizedGuideScores) > Q[2]),]
        
        return(list(as.numeric(bot$standardizedGuideScores) , as.numeric(top$standardizedGuideScores)))
      }
    }
    else{
      Q = quantile(as.numeric(x$standardizedGuideScores) , probs = c(BOTTOM,TOP))
      bot = x[which(as.numeric(x$standardizedGuideScores) < Q[1]),]
      top = x[which(as.numeric(x$standardizedGuideScores) > Q[2]),]
      
      return(list(as.numeric(bot$standardizedGuideScores) , as.numeric(top$standardizedGuideScores)))
    }
  }
}

GetQ4fraction=function(x = All[[1]]){
  if (is.null(x) == T){
    return(NA)
  }
  else{
    
    q = which(x$quartile == 4)

    out = rep("NA",25)
    names(out) = seq(1,25,1)
    
    if(length(q) == 0){
      return(out)
    }
    if(length(q) > 24){
      out[1:25] = TRUE
      return(out)
    }
    else{
      out[1:length(q)] = TRUE
      return(out)
    }
  }
}


########################## sub-routines end  ########

########## execute ##################################


# name Guide prediction files
names(CSVs) = sapply( files , GetName )

# Match names to reference 
# ! only transcripts with > 100nt are considered
idx = match(names(CSVs) , Ref$ENST)
if (all( names(CSVs) == Ref$ENST[idx])){
  Ref = Ref[idx,]
  Ref$UTR5 = gsub("UTR5:","",Ref$UTR5)
  Ref$CDS = gsub("CDS:","",Ref$CDS)
  Ref$UTR3 = gsub("UTR3:","",Ref$UTR3)
  Ref$CDS.start <- sapply( Ref$CDS , GetCoord , y=1 )
  Ref$CDS.end <- sapply( Ref$CDS , GetCoord , y=2 )
  rownames(Ref) <- Ref$ENST
}else{
  stop("Exiting! File names don't match. Please check file names.")
}




if( !(file.exists("./results/GuideAnnotaion.RData")) ){
  
  cat( paste0( "fetching guide info started on " , date() , "\n"))
  cat( "This will take a while....\n\n")
  GuideAnnotaion = list()
  cnt=0
  for (i in 1:length(CSVs)){
    cnt=cnt+1
    if (  cnt%%1000 == 0){
      cat(paste0(cnt," of ",length(CSVs), " transcripts processed - ", date() ,"\n" ))
    }
    
    GuideAnnotaion[[i]] = AnnotateGuides(x = CSVs[[i]],ref = Ref[names(CSVs)[i],])
    names(GuideAnnotaion)[i] = names(CSVs)[i] 
  }
  cat( paste0( "fetching guide info ended on " , date(),"\n"))
  
  
  # Save data
  save(GuideAnnotaion, file = "./results/GuideAnnotaion.RData")
  
}else{
  
  cat( paste0( "loading pre-computed files started on " , date() , "\n"))
  
  load(file = "./results/GuideAnnotaion.RData") # loads object called "GuideAnnotaion"
}



IDs_ProteinCoding = unique(sapply(GTF$V9,GetTxIDs))

# testing
# IDs_ProteinCoding = IDs_ProteinCoding[which(IDs_ProteinCoding %in% names(CSVs))]

set.seed(1234)
SampleSize = 1000 #25 testing
GuideAnnotaion.pc = GuideAnnotaion[IDs_ProteinCoding]
S = sample(1:length(GuideAnnotaion.pc), size = SampleSize, replace = F )
GuideAnnotaion.sub = GuideAnnotaion.pc[S]



cat( paste0( "\nGet top/bottom guide score distribution " , date() , "\n"))
p=list()
for (i in 1:length(names(GuideAnnotaion.sub[[1]]))){
  
  cat = names(GuideAnnotaion.sub[[1]])[i]
  
  
  TB = lapply(GuideAnnotaion.sub, GetTopBot,TOP = 0.75, BOTTOM = 0.25 , NAME = cat)
  bot = melt(unlist(lapply(TB , FUN = function(x){if (length(x) == 2){x[[1]]} else {NA}})))
  top = melt(unlist(lapply(TB , FUN = function(x){if (length(x) == 2){x[[2]]} else {NA}})))
  del = which(is.na(bot) == T)
  if (length(del > 0)){
    bot = data.frame(bot[-del,])
    colnames(bot)="value"
  }
  del = which(is.na(top) == T)
  if (length(del > 0)){
    top = data.frame(top[-del,])
    colnames(top)="value"
  }
  bot$Q = "<25%"
  top$Q = ">75%"
  cat( paste0( "\n95% conficende interval given top 25% guides per ",cat,"\n"))
  q=quantile(top$value, probs = c(0.025,0.975)) 
  cat( paste0(names(q),"=",signif( q, 3), "\n"))
  df = rbind.data.frame(bot,top)
  p[[i]] = ggplot(df, aes(x=value, color = Q,fill = Q)) + 
    geom_density() +
    theme_classic() + 
    scale_color_manual(values=c( "#000000" , "#000000")) +
    scale_fill_manual(values=c( "#de2d26" , "#3182bd")) +
    xlab("Predicted Guide score")+
    ggtitle(cat) +
    xlim(c(0,1)) +
    ylim(c(0,20)) +
    coord_fixed(1/20) +
    geom_vline(xintercept = q , colour="#525252", linetype = "dashed") +
    annotate('text' , x = q[2]-( q[2] - q[1])/2 , y = 17.5 , label = "95%\nconf. int.", hjust = 0.5, vjust=0.5, size = 3)
  
  names(p)[i] = cat
}
pdf(paste0("./figures/ScoreDistribution_25-75.pdf"), width = 12, height = 3, useDingbats = F)
grid.arrange(arrangeGrob(grobs= p,ncol=4))
dev.off()








# Split by annotation category 
cat( paste0( "split guide info by annotation started on " , date() , "\n"))
UTR5s=lapply( GuideAnnotaion, FUN=function(x){x[[1]]})
CDSs=lapply( GuideAnnotaion, FUN=function(x){x[[2]]})
UTR3s=lapply( GuideAnnotaion, FUN=function(x){x[[3]]})
All=lapply( GuideAnnotaion, FUN=function(x){x[[4]]})



# get fraction of genes harboring 5'utr, cds and 3'utr 
cat( paste0( "Get annotation fraction started on " , date() , "\n"))
UTR5s.fractions = sapply(UTR5s,GetPredictionFraction)
CDSs.fractions = sapply(CDSs,GetPredictionFraction)
UTR3s.fractions = sapply(UTR3s,GetPredictionFraction)

UTR5s.NAs = table(sapply( UTR5s.fractions , is.na) ) / length(UTR5s.fractions)
CDSs.NAs = table(sapply( CDSs.fractions , is.na) ) / length(CDSs.fractions)
UTR3s.NAs = table(sapply( UTR3s.fractions , is.na) ) / length(UTR3s.fractions)

df = data.frame(matrix(0, ncol = 3 , nrow = 3))
colnames(df) = c("Annotation", "true" , "false")
df$Annotation <- c("5'utr","cds","3'utr")
df[1,2:3] = UTR5s.NAs[c("FALSE","TRUE")]
if (length(CDSs.NAs) == 1){
  df[2,2] = CDSs.NAs
}else{
  df[2,2:3] = CDSs.NAs[c("FALSE","TRUE")] 
}
df[3,2:3] = UTR3s.NAs[c("FALSE","TRUE")]
df = melt(df[,1:2] , id.vars = "Annotation")
colnames(df)[2] = "exists"
df$Annotation = factor( df$Annotation , levels = c("5'utr","cds","3'utr"))
df$exists = factor( df$exists , levels = c("false","true"))

# Proportion of genes with Guides in 5'UTR / CDS / 3'UTR
pdf("./figures/TranscriptAnnotation.pdf", width = 2.5, height = 3, useDingbats = F)
ggplot(df , aes( x = Annotation , y = value, fill = Annotation)) + 
  geom_bar(stat="identity", position="stack", width = 0.5) +
  theme_classic() + 
  ylab("Fraction")+
  scale_fill_manual(values=c( "#fec44f", "#3182bd", "#de2d26")) +
  theme(legend.position = "none")
dev.off()




# I am not returning any guides with NA scores...
# NAs only occur if the entire mRNA subannotation category has no valid prediction
# return the fraction of annotation category harboring potential crRNA guides
# Guides per 5'UTR / CDS / 3'UTR
# cat( paste0( "Get guide fraction per annotation category started on " , date() , "\n"))
# L = list(UTR5s.fractions, CDSs.fractions, UTR3s.fractions)
# names(L) = c("5'utr","cds","3'utr")
# df = melt(L)
# colnames(df) = c("Fraction","Annotation")
# df$Annotation = factor( df$Annotation , levels = c("5'utr","cds","3'utr"))
# pdf("./figures/TranscriptAnnotationFractions.pdf", width = 2.5, height = 3, useDingbats = F)
# ggplot(df , aes( x = Annotation , y = Fraction, fill = Annotation)) + 
#   geom_boxplot(outlier.size = -1, width = 0.5) +
#   theme_classic() + 
#   ylim(c(0,1))+
#   scale_fill_manual(values=c( "#fec44f", "#3182bd", "#de2d26"))
# dev.off()





# return predicted sores for potential crRNA guides
# Scores per 5'UTR / CDS / 3'UTR
UTR5s.scores = unlist(sapply(UTR5s ,FUN = function(x){na.omit(suppressWarnings(as.numeric(x[,"standardizedGuideScores"])))}))
CDSs.scores = unlist(sapply(CDSs ,FUN = function(x){na.omit(suppressWarnings(as.numeric(x[,"standardizedGuideScores"])))}))
UTR3s.scores = unlist(sapply(UTR3s ,FUN = function(x){na.omit(suppressWarnings(as.numeric(x[,"standardizedGuideScores"])))}))

S = list(UTR5s.scores ,CDSs.scores , UTR3s.scores)
names(S) =  c("5'utr","cds","3'utr")
df = melt(S)
colnames(df) = c("GuideScore","Annotation")
df$Annotation = factor( df$Annotation , levels = c("5'utr","cds","3'utr"))

g = ggplot(df , aes( x = Annotation , y = GuideScore, fill = Annotation)) +
  geom_boxplot(outlier.size = -1) +
  theme_classic() +
  ylim(c(0,1))+
  scale_fill_manual(values=c( "#fec44f", "#3182bd", "#de2d26"))




# get quartiles of predicted guides relative to GFP screen for 5'utr, cds and 3'utr 
cat( paste0( "Get prediction quartiles started on " , date() , "\n"))
UTR5s.Q = unlist(sapply(UTR5s[which(names(UTR5s) %in% IDs_ProteinCoding)],GetPredictionQuartile))
CDSs.Q = unlist(sapply(CDSs[which(names(UTR5s) %in% IDs_ProteinCoding)],GetPredictionQuartile))
UTR3s.Q = unlist(sapply(UTR3s[which(names(UTR5s) %in% IDs_ProteinCoding)],GetPredictionQuartile))
all.Q = c(UTR5s.Q,CDSs.Q,UTR3s.Q)


df = cbind.data.frame(table(all.Q),table(UTR5s.Q),table(CDSs.Q),table(UTR3s.Q))
df = df[,c(1,2,4,6,8)]
colnames(df) = c("Quartile", "Tx", "5'utr", "cds", "3'utr")
df.norm = data.frame(apply(df[2:5], 2, FUN = function(x){x/sum(x)}))
df.norm$Quartile = df$Quartile
colnames(df.norm) = c( "Tx", "5'utr", "cds", "3'utr","Quartile")
df = melt(df.norm , id.vars = "Quartile")
df$Quartile = factor(df$Quartile , levels = rev(c("1","2","3","4")))
write.table(df , file = "./results/PredictionQuartiles.txt", col.names = F, row.names = F, sep = "\t", quote = F)

pdf("./figures/FigS11b_PredictionQuartiles.pdf", width = 2.5, height = 3, useDingbats = F)
ggplot(df , aes( x = variable , y = value, fill = Quartile)) + 
  geom_bar(stat="identity", position="stack") +
  theme_classic() + 
  ylab("Fraction") +
  ylab("") +
  scale_fill_manual(values=rev(c( "#bdd7e7" , "#6baed6" ,"#3182bd" ,"#08519c" )))
dev.off()


# How many genes have top quartile prediction?

Q4.all.fractions = lapply(All,GetQ4fraction)
Q4.UTR5.fractions = lapply(UTR5s,GetQ4fraction)
Q4.CDS.fractions = lapply(CDSs,GetQ4fraction)
Q4.UTR3.fractions = lapply(UTR3s,GetQ4fraction)

Q4.all.ma = do.call(rbind,Q4.all.fractions)
Q4.UTR5.ma = do.call(rbind,Q4.UTR5.fractions)
Q4.CDS.ma = do.call(rbind,Q4.CDS.fractions)
Q4.UTR3.ma = do.call(rbind,Q4.UTR3.fractions)

L = list()
L[[1]] = apply(Q4.UTR5.ma, 2 , FUN = function(x){length(grep("TRUE",x))/length(x)} )
L[[2]] = apply(Q4.CDS.ma, 2 , FUN = function(x){length(grep("TRUE",x))/length(x)} )
L[[3]] = apply(Q4.UTR3.ma, 2 , FUN = function(x){length(grep("TRUE",x))/length(x)} )
L[[4]] = apply(Q4.all.ma, 2 , FUN = function(x){length(grep("TRUE",x))/length(x)} )
names(L) = c("5'utr" ,"cds",  "3'utr", "all" )
df = melt(L)
df$N = rep(seq(1,25,1), times = 4)
colnames(df)[2] = "Annotation"
df$Annotation = factor(df$Annotation , levels = c(c("all", "5'utr" ,"cds",  "3'utr" )) )
write.table(df, file = "./results/Q4guides_fraction.txt", row.names = F, col.names = T, quote = F, sep = "\t")

pdf("./figures/FigS11c_Q4guides_fraction.pdf", width = 6, height = 3, useDingbats = F)
ggplot(df , aes( x = N, y = value, color = Annotation)) + 
  geom_line() + 
  geom_point() + 
  ylim(c(0,1)) + 
  theme_classic() + 
  coord_fixed(r = 25) + 
  xlab("# Q4 guides") +
  ylab("fraction of all transcripts") +
  scale_color_manual(values=c("#525252" ,"#fec44f", "#3182bd", "#de2d26")) +
  scale_x_continuous(breaks = c(1,5,10,15,20,25), labels=as.character(c(1,5,10,15,20,25)))
dev.off()









# retrieve the random/top n guides per gene per annotation category
# get 10 random guides scores per gene per annotation category
cat( paste0( "Get random guide scores started on " , date() , "\n"))
all.scores = na.omit(unlist(lapply(All , FUN = GetTopGuides , n = 10, WHICH = "random", WHAT = "scores")))
UTR5s.scores = na.omit(unlist(lapply(UTR5s , FUN = GetTopGuides , n = 10, WHICH = "random", WHAT = "scores")))
CDSs.scores = na.omit(unlist(lapply(CDSs , FUN = GetTopGuides , n = 10, WHICH = "random", WHAT = "scores")))
UTR3s.scores = na.omit(unlist(lapply(UTR3s , FUN = GetTopGuides , n = 10, WHICH = "random", WHAT = "scores")))

S = list(all.scores, UTR5s.scores ,CDSs.scores , UTR3s.scores)
names(S) =  c("all", "5'utr","cds","3'utr")
df = melt(S)
colnames(df) = c("GuideScore","Annotation")
df$Annotation = factor( df$Annotation , levels = c("all" ,"5'utr","cds","3'utr"))
#pdf("TranscriptAnnotationGuideScoresRandom.pdf", width = 3, height = 3, useDingbats = F)
g1 = ggplot(df , aes( x = Annotation , y = GuideScore, fill = Annotation)) + 
  geom_violin() +
  geom_boxplot(outlier.size = -1, width = 0.1, fill = "white") +
  theme_classic() + 
  ylim(c(0,1)) +
  ggtitle("10 randomly\nselected crRNAs") +
  scale_fill_manual(values=c( "#525252","#fec44f", "#3182bd", "#de2d26"))+
  theme(plot.title = element_text(size = 10))
#dev.off()


# get 10 top guides scores per gene per annotation category
cat( paste0( "Get top guide scores started on " , date() , "\n"))
all.scores = na.omit(unlist(lapply(All , FUN = GetTopGuides , n = 10, WHICH = "top", WHAT = "scores")))
UTR5s.scores = na.omit(unlist(lapply(UTR5s , FUN = GetTopGuides , n = 10, WHICH = "top", WHAT = "scores")))
CDSs.scores = na.omit(unlist(lapply(CDSs , FUN = GetTopGuides , n = 10, WHICH = "top", WHAT = "scores")))
UTR3s.scores = na.omit(unlist(lapply(UTR3s , FUN = GetTopGuides , n = 10, WHICH = "top", WHAT = "scores")))

S = list(all.scores, UTR5s.scores ,CDSs.scores , UTR3s.scores)
names(S) =  c("all", "5'utr","cds","3'utr")
DF = melt(S)
colnames(DF) = c("GuideScore","Annotation")
DF$Annotation = factor( DF$Annotation , levels = c("all" ,"5'utr","cds","3'utr"))
#pdf("TranscriptAnnotationGuideScoresTop.pdf", width = 3, height = 3, useDingbats = F)
g2 = ggplot(DF , aes( x = Annotation , y = GuideScore, fill = Annotation)) + 
  geom_violin() +
  geom_boxplot(outlier.size = -1, width = 0.1, fill = "white") +
  theme_classic() + 
  ylim(c(0,1)) +
  ggtitle("top 10\ncrRNAs")+
  scale_fill_manual(values=c( "#525252","#fec44f", "#3182bd", "#de2d26"))+
  theme(plot.title = element_text(size = 10))
#dev.off()

pdf(paste0("./figures/FigS11a_TranscriptAnnotationGuideScores.pdf"), width = 6, height = 3, useDingbats = F)
grid.arrange(arrangeGrob(grobs= list(g1,g2),ncol=2))
dev.off()



# get 10 top guides scores per gene per annotation category
cat( paste0( "Prepare output started on " , date() , "\n"))
UTR5s.guides = lapply(UTR5s , FUN = GetTopGuides , n = 10, WHICH = "top", WHAT = "guides")
CDSs.guides = lapply(CDSs , FUN = GetTopGuides , n = 10, WHICH = "top", WHAT = "guides")
UTR3s.guides = lapply(UTR3s , FUN = GetTopGuides , n = 10, WHICH = "top", WHAT = "guides")

UTR5s.guides.df = do.call(rbind,UTR5s.guides)
CDSs.guides.df = do.call(rbind,CDSs.guides)
UTR3s.guides.df = do.call(rbind,UTR3s.guides)

rownames(UTR5s.guides.df) = names(UTR5s.guides)
rownames(CDSs.guides.df) = names(CDSs.guides)
rownames(UTR3s.guides.df) = names(UTR3s.guides)

if( all(rownames(UTR5s.guides.df) == rownames(CDSs.guides.df) & rownames(UTR5s.guides.df) == rownames(UTR3s.guides.df)) ){
 guides.df = cbind(UTR5s.guides.df,CDSs.guides.df,UTR3s.guides.df)
 colnames(guides.df) = c( paste0(c("UTR5_crRNA"), "_" ,c(1:10)) , paste0(c("CDS_crRNA"), "_" ,c(1:10)) , paste0(c("UTR3_crRNA"), "_" ,c(1:10)))
}





# Match names to reference 
# ! only transcripts with > 100nt are considered
idx = match(rownames(guides.df) , Ref$ENST)
if (all( rownames(guides.df) == Ref$ENST[idx])){
  Ref = cbind(Ref[idx,1:10],guides.df)
  rownames(Ref) <- NULL
}else{
  stop("Exiting! File names don't match. Please check file names.")
}
cat( paste0( "Write output started on " , date() , "\n"))
write.table(Ref, file = "./results/gencode.v19.Cas13d_crRNAs.txt", row.names = F, col.names = T, quote = F, sep = "\t")

cat( paste0( "finished at " , date() , "\n"))
cat( paste0( "\ntop predicted crRNAs have been written to \"gencode.v19.Cas13d_crRNAs.txt\"\n\n")) #Supplementary Data 8
