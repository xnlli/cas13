#!/bin/bash
#$ -cwd

SCRIPTS="./scripts/"
FILES="./data/"

# Download Gencode protein coding transcript sequences, if needed
if [[ ! -f "${FILES}gencode.v19.pc_transcripts.fa" ]]; then 

	wget ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_19/gencode.v19.pc_transcripts.fa.gz
	gunzip gencode.v19.pc_transcripts.fa.gz
	mv gencode.v19.pc_transcripts.fa ${FILES}
	less ${FILES}gencode.v19.pc_transcripts.fa | grep ">"  >  ${FILES}gencode.v19.pc_transcripts.txt

	wget ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_19/gencode.v19.annotation.gtf.gz
	less gencode.v19.annotation.gtf.gz| grep "transcript_type \"protein_coding\"" > gencode.v19.annotation.protein_coding.txt
	rm gencode.v19.annotation.gtf.gz
	mv gencode.v19.annotation.protein_coding.txt ${FILES}

fi

# Split transcript sequence file into individual transcript.fasta sequences.
bash ${SCRIPTS}SplitFA.sh ${FILES}gencode.v19.pc_transcripts.fa



# The number of resulting fasta files is 99349 
# Thus, processing all files will be split into 10 sub-directories based on the transcript IDs

ids=$(echo {0000000..0000069})
for id in $ids; do
	mkdir -p ./${FILES}/ENST${id}
done 

for id in $ids; do
	mv ENST${id}* ./${FILES}/ENST${id}
done 

# remove empty folders
find ./${FILES}/ -type d -empty -delete


exit



