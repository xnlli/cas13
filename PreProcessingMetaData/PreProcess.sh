#!/bin/bash
#$ -cwd

SCRIPTS="./scripts/"
FILES="./data/"


########## step 1
# calculates the probability of nucleotides being unpaired to assess target site accessibility
# This approach is similar to Agarval et al. 2018 Genome Biology
# Run RNAplfold if needed
if [[ ! -f "${FILES}pHKO32_GFP_lunp" ]]; then 
	echo -e "\t predicting RNA structure for GFP"
	cd ./data/
	# You will need to install RNAfold from the vienna package https://www.tbi.univie.ac.at/RNA/
	# Here I am providing a RNAplfold linux binary from Version 2.4.10
	cat GFP_ext.fa | ../scripts/RNAplfold -L 40 -W 80 -u 50
	rm *_dp.ps
	cd ..
fi
if [[ ! -f "${FILES}ENST00000367042.1_lunp" ]]; then 
	echo -e "\t predicting RNA structure for CD46"
	cd ./data/
	cat CD46_ENST00000367042.1.fa | ../scripts/RNAplfold -L 40 -W 80 -u 50
	rm *_dp.ps
	cd ..
fi
if [[ ! -f "${FILES}ENST00000367064.3_lunp" ]]; then 
	echo -e "\t predicting RNA structure for CD55"
	cd ./data/
	cat CD55_ENST00000367064.3.fa | ../scripts/RNAplfold -L 40 -W 80 -u 50
	rm *_dp.ps
	cd ..
fi
if [[ ! -f "${FILES}ENST00000360110.4_lunp" ]]; then 
	echo -e "\t predicting RNA structure for CD71"
	cd ./data/
	cat CD71_ENST00000360110.4.fa | ../scripts/RNAplfold -L 40 -W 80 -u 50
	rm *_dp.ps
	cd ..
fi





########## step 2
# calculates folding and minimum free energy for mature crRNAs
# Run RNAfold if needed
if [[ ! -f "${FILES}Cas13d_GFP_library.final.crRNAplusDR.foldedTransformed.fa" ]]; then 
	echo -e "\t predicting structure for GFP crRNAs"
	cd ./data/
	# You will need to install RNAfold from the vienna package https://www.tbi.univie.ac.at/RNA/
	# Here I am providing a RNAfold linux binary from Version 2.4.10
	bash ../scripts/GetMFEforCas13guides.sh Cas13d_GFP_library.final.fa 27
	rm *crRNAplusDR.fa *crRNAplusDR.folded.fa
	cd ..
fi
if [[ ! -f "${FILES}Cas13d_GFP_library.final.crRNAplusDR.23mer.foldedTransformed.fa" ]]; then 
	echo -e "\t predicting structure for GFP crRNAs as 23mers"
	# Reduce guide size to 23mer. Needed for Model building in Figure 3
	cd ./data/
	bash ../scripts/GetMFEforCas13guides.sh Cas13d_GFP_library_23mer.final.fa 23
	rm *crRNAplusDR.fa *crRNAplusDR.folded.fa
	cd ..
fi
if [[ ! -f "${FILES}CD46_library.final.crRNAplusDR.foldedTransformed.fa" ]]; then 
	echo -e "\t predicting structure for CD46 crRNAs"
	cd ./data/
	bash ../scripts/GetMFEforCas13guides.sh CD46_library.final.fa 23
	rm *crRNAplusDR.fa *crRNAplusDR.folded.fa
	cd ..
fi
if [[ ! -f "${FILES}CD55_library.final.crRNAplusDR.foldedTransformed.fa" ]]; then 
	echo -e "\t predicting structure for CD55 crRNAs"
	cd ./data/
	bash ../scripts/GetMFEforCas13guides.sh CD55_library.final.fa 23
	rm *crRNAplusDR.fa *crRNAplusDR.folded.fa
	cd ..
fi
if [[ ! -f "${FILES}CD71_library.final.crRNAplusDR.foldedTransformed.fa" ]]; then 
	echo -e "\t predicting structure for CD71 crRNAs"
	cd ./data/
	bash ../scripts/GetMFEforCas13guides.sh CD71_library.final.fa 23
	rm *crRNAplusDR.fa *crRNAplusDR.folded.fa
	cd ..
fi








########## step 3
# calculates the RNA-hybridization energy (MFE) between crRNA spacer and the cognate target site
# The calculation the MFE for all ~7000 guides including all sub parts (n=378 for 27mers), will take some time (~35hrs)
# Precomputed files are provided
# Run RNAhybrid if needed
if [[ ! -f "${FILES}GFP_HybridizationMatrix_merged_All.csv" ]]; then 
	echo -e "\t predicting guide::target hybridization energies for GFP"
	cd ./data/
	# A) Generate an alignmentfile of the crRNA library against its target using bowtie (you may need to install bowtie). 
	# Precomputed index files are provided.
	# This is only done for guides that map to the mature transcript. Special cases such as intron-targeting guides are ignored.
	module purge
	module load bowtie
	IDX='./BowtieIndex/GFP/GFP'
	bowtie -v 3 -f ${IDX} Cas13d_GFP_library.final.fa --un GFP_library.unmapped.fa -S GFP_library.Inputmatch.sam 2> BowtieLog.GFP_Inputmatch.txt
	# B) transform sam to bam (You may need to install samtools)
	module load samtools
	samtools view -bhS GFP_library.Inputmatch.sam | samtools sort -o - - > GFP_library.Inputmatch.bam
	samtools index GFP_library.Inputmatch.bam

	# C)
	# You will need to install RNAhybrid # https://bibiserv.cebitec.uni-bielefeld.de/rnahybrid?id=rnahybrid_view_download and add RNAhybrid to you ~/local/bin/
	# Then, you need to change the path from where RNAhybrid is called within CalculateRNAhybrid.R # e.g. ~/local/bin/RNAhybrid -c -s 3utr_worm
	# Also note, I overwrite 3UTR_worm.freq within path/to/RNAhybrid-2.1.2/examples/ and use it in ~/bin/RNAhybrid -c -s 3utr_worm. This is because RNAhybrid was inflexible in allowing for a custom dinucleotide frequency file. 
	# We used RNAhybrid version 2.1.2
	# .... it will take some time if you do this for all guides (~35hrs) 
	# better, restrict this to only the perfect match guide RNAs
	echo "../scripts/CalculateRNAhybrid.R GFP.fa GFP_library.Inputmatch.bam 27 GFP" | qsub -V -cwd -j y -N RNAhyb -l mem=64G # -e /dev/null -o /dev/null 
	cd ..
fi

if [[ ! -f "${FILES}CD46_HybridizationMatrix_merged_All.csv" ]]; then 
	echo -e "\t predicting guide::target hybridization energies for CD46"
	cd ./data/
	# A) Generate an alignmentfile of the crRNA library against its target using bowtie (you may need to install bowtie). 
	# Precomputed index files are provided.
	# This is only done for guides that map to the mature transcript. Special cases such as intron-targeting guides are ignored.
	# remove intron-targting, guide LengthVariant and RevComp before mapping
	less CD46_library.final.fa | awk '/^>/ {printf("%s%s\t",(N>0?"\n":""),$0);N++;next;} {printf("%s",$0);} END {printf("\n");}' | grep -v -E '>rc_|intron|RevCom|LengthVariant'| awk '{print $1"\n"$2}' > tmp.CD46_library.final.fa
	module purge
	module load bowtie
	IDX='./BowtieIndex/CD46/CD46'
	bowtie -v 3 -f ${IDX} tmp.CD46_library.final.fa --un CD46_library.unmapped.fa -S CD46_library.Inputmatch.sam 2> BowtieLog.CD46_Inputmatch.txt
	
	# B) transform sam to bam (You may need to install samtools)
	module load samtools
	samtools view -bhS CD46_library.Inputmatch.sam | samtools sort -o - - > CD46_library.Inputmatch.bam
	samtools index CD46_library.Inputmatch.bam
	
	rm tmp.CD46_library.final.fa

	# C)
	# You will need to install RNAhybrid # https://bibiserv.cebitec.uni-bielefeld.de/rnahybrid?id=rnahybrid_view_download and add RNAhybrid to you ~/local/bin/
	# Then, you need to change the path from where RNAhybrid is called within CalculateRNAhybrid.R # e.g. ~/local/bin/RNAhybrid -c -s 3utr_worm
	# Also note, I overwrite 3UTR_worm.freq within path/to/RNAhybrid-2.1.2/examples/ and use it in ~/bin/RNAhybrid -c -s 3utr_worm. This is because RNAhybrid was inflexible in allowing for a custom dinucleotide frequency file. 
	# We used RNAhybrid version 2.1.2
	# .... it will take some time if you do this for all guides (~35hrs) 
	# better, restrict this to only the perfect match guide RNAs
	echo "../scripts/CalculateRNAhybrid.R CD46_ENST00000367042.1.fa CD46_library.Inputmatch.bam 23 CD46" | qsub -V -cwd -j y -N RNAhyb -l mem=64G # -e /dev/null -o /dev/null 
	cd ..
fi

if [[ ! -f "${FILES}CD55_HybridizationMatrix_merged_All.csv" ]]; then 
	echo -e "\t predicting guide::target hybridization energies for CD55"
	cd ./data/
	# A) Generate an alignmentfile of the crRNA library against its target using bowtie (you may need to install bowtie). 
	# Precomputed index files are provided.
	# This is only done for guides that map to the mature transcript. Special cases such as intron-targeting guides are ignored.
	# remove intron-targting, guide LengthVariant and RevComp before mapping
	less CD55_library.final.fa | awk '/^>/ {printf("%s%s\t",(N>0?"\n":""),$0);N++;next;} {printf("%s",$0);} END {printf("\n");}' | grep -v -E '>rc_|intron|RevCom|LengthVariant'| awk '{print $1"\n"$2}' > tmp.CD55_library.final.fa
	
	module purge
	module load bowtie
	IDX='./BowtieIndex/CD55/CD55'
	bowtie -v 3 -f ${IDX} tmp.CD55_library.final.fa --un CD55_library.unmapped.fa -S CD55_library.Inputmatch.sam 2> BowtieLog.CD55_Inputmatch.txt

	# B) transform sam to bam (You may need to install samtools)
	module load samtools
	samtools view -bhS CD55_library.Inputmatch.sam | samtools sort -o - - > CD55_library.Inputmatch.bam
	samtools index CD55_library.Inputmatch.bam

	rm tmp.CD55_library.final.fa

	# C)
	# You will need to install RNAhybrid # https://bibiserv.cebitec.uni-bielefeld.de/rnahybrid?id=rnahybrid_view_download and add RNAhybrid to you ~/local/bin/
	# Then, you need to change the path from where RNAhybrid is called within CalculateRNAhybrid.R # e.g. ~/local/bin/RNAhybrid -c -s 3utr_worm
	# Also note, I overwrite 3UTR_worm.freq within path/to/RNAhybrid-2.1.2/examples/ and use it in ~/bin/RNAhybrid -c -s 3utr_worm. This is because RNAhybrid was inflexible in allowing for a custom dinucleotide frequency file. 
	# We used RNAhybrid version 2.1.2
	# .... it will take some time if you do this for all guides (~35hrs) 
	# better, restrict this to only the perfect match guide RNAs
	echo "../scripts/CalculateRNAhybrid.R CD55_ENST00000367064.3.fa CD55_library.Inputmatch.bam 23 CD55" | qsub -V -cwd -j y -N RNAhyb -l mem=64G # -e /dev/null -o /dev/null 
	cd ..
fi

if [[ ! -f "${FILES}CD71_HybridizationMatrix_merged_All.csv" ]]; then 
	echo -e "\t predicting guide::target hybridization energies for CD71"
	cd ./data/
	# A) Generate an alignmentfile of the crRNA library against its target using bowtie (you may need to install bowtie). 
	# Precomputed index files are provided.
	# This is only done for guides that map to the mature transcript. Special cases such as intron-targeting guides are ignored.
	# remove intron-targting, guide LengthVariant and RevComp before mapping
	less CD71_library.final.fa | awk '/^>/ {printf("%s%s\t",(N>0?"\n":""),$0);N++;next;} {printf("%s",$0);} END {printf("\n");}' | grep -v -E '>rc_|intron|RevCom|LengthVariant'| awk '{print $1"\n"$2}' > tmp.CD71_library.final.fa
	
	module purge
	module load bowtie
	IDX='./BowtieIndex/CD71/CD71'
	bowtie -v 3 -f ${IDX} tmp.CD71_library.final.fa --un CD71_library.unmapped.fa -S CD71_library.Inputmatch.sam 2> BowtieLog.CD71_Inputmatch.txt
	
	# B) transform sam to bam (You may need to install samtools)
	module load samtools
	samtools view -bhS CD71_library.Inputmatch.sam | samtools sort -o - - > CD71_library.Inputmatch.bam
	samtools index CD71_library.Inputmatch.bam

	rm tmp.CD71_library.final.fa

	# C)
	# You will need to install RNAhybrid # https://bibiserv.cebitec.uni-bielefeld.de/rnahybrid?id=rnahybrid_view_download and add RNAhybrid to you ~/local/bin/
	# Then, you need to change the path from where RNAhybrid is called within CalculateRNAhybrid.R # e.g. ~/local/bin/RNAhybrid -c -s 3utr_worm
	# Also note, I overwrite 3UTR_worm.freq within path/to/RNAhybrid-2.1.2/examples/ and use it in ~/bin/RNAhybrid -c -s 3utr_worm. This is because RNAhybrid was inflexible in allowing for a custom dinucleotide frequency file. 
	# We used RNAhybrid version 2.1.2
	# .... it will take some time if you do this for all guides (~35hrs) 
	# better, restrict this to only the perfect match guide RNAs
	echo "../scripts/CalculateRNAhybrid.R CD71_ENST00000360110.4.fa CD71_library.Inputmatch.bam 23 CD71" | qsub -V -cwd -j y -N RNAhyb -l mem=64G # -e /dev/null -o /dev/null 
	cd ..
fi


exit


