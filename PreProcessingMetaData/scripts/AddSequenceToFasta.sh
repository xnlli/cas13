#!/bin/bash
#$ -l mem_free=8G,h_vmem=12G
#$ -cwd

#############################
# Usage


rflag=false
usage () {  echo -e "\n\tThis script adds a leader and/or downstream sequence to an input Fasta sequence"
			echo -e "\trun: bash ~/scripts/AddSequenceToFasta.sh -i <input.fa> -u <sequence string> -d <sequence string>  >  output.fa"
			echo -e "\n\tExiting! Please use the following options:\n"
			
  			echo -e "\t    -i     Input fasta file (i.e. prefix.fa with crRNA sequences) - required"
        	echo -e "\t    -u     upstream sequence \"string\" (e.g. Cas13d Direct Repeat sequence) - required"
        	echo -e "\t    -d     downstream sequence \"string\" (e.g. downstream gibson overhang) - optional\n"; }


set -e

if (($# == 0)); then
  usage
exit 1
fi

while getopts ":i:u:d:h" opt; do
  case $opt in
    i)	rflag=true;FASTA=$OPTARG;;
    u)  rflag=true;UPSTREAM=$OPTARG;;
	d)  rflag=true;DOWNSTREAM=$OPTARG;;
    h)	usage; exit 1;;
    \?)	usage; exit 1;;
    :)	usage; exit 1;;
  esac
done


shift $(($OPTIND - 1))

if ! $rflag && [[ -d $1 ]]
then
    usage >&2
    exit 1
fi

if [[ -z "$FASTA" || -z "$UPSTREAM" ]]; then
    usage >&2
    exit 1
fi

startChar=$(head $FASTA -n 1 | awk -F"\t" 'BEGIN{OFS=FS}{$1 = substr($1,1,1); print $1}')

if [ "$startChar" != ">" ]; then

	echo -e "\n\tInput Fasta File does not start with \">\" "
    usage >&2
    exit 1
fi

#-------------------------------------------

if [[ -z "$DOWNSTREAM" ]];then


	cat $FASTA | awk -v UPSEQ=$UPSTREAM '/^>/ { if(i>0) printf("\n"); i++; printf("%s\t",$0); next;} {printf("%s",UPSEQ$0);} END { printf("\n");}'| awk '{printf("%s\n%s\n",$1,$2)}' 

elif [[ ! -z "$DOWNSTREAM" ]];then



	cat $FASTA | awk -v UPSEQ=$UPSTREAM -v DOWNSEQ=$DOWNSTREAM '/^>/ { if(i>0) printf("\n"); i++; printf("%s\t",$0); next;} {printf("%s",UPSEQ$0DOWNSEQ);} END { printf("\n");}'| awk '{printf("%s\n%s\n",$1,$2)}'

fi 

exit