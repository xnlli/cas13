#!/usr/bin/env Rscript

###### set variables; load libs #####################
.libPaths(new="~/R/x86_64-redhat-linux-gnu-library/3.5")
suppressMessages(library(reshape2, quietly = T))
suppressMessages(library(ggplot2, quietly = T))
suppressMessages(library(Biostrings, quietly = T))
suppressMessages(library(grid, quietly = T))
suppressMessages(library(gridExtra, quietly = T))
suppressMessages(library(pheatmap, quietly = T))
suppressMessages(library(colorRamps, quietly = T))
suppressMessages(library(ppcor, quietly = T))



###### sub-routines #################################

# get position of match start
extractLength <- function(y){
  coord <- strsplit( strsplit(y, split = ":")[[1]][2] ,split = "_")[[1]][1]
  e = as.integer(strsplit(coord, split = "-")[[1]][1])
  s = as.integer(strsplit(coord, split = "-")[[1]][2])
  return(s-e+1)
}

PlotGuideLengthEffects = function(x , value = "meanCS" , TITLE = "", LVL = NULL , MORE = FALSE){
  
  gl = x[grep("Length Variant", x$MatchType),]
  pm = x[which( x$Guide %in% gl$Guide & x$MatchType == "Perfect Match"),]
  GL = rbind.data.frame(pm,gl)
  
  # split by Length
  LEN <- split(GL, f = GL$GuideLength)
  
  # keep only CRISPR scores
  LEN.vals = lapply(LEN, FUN=function(y){y[,c("Guide",value)]})
  
  # transform
  df = melt(LEN.vals , id.vars = c("Guide",value))
  colnames(df) = c("Guide","value", "Length")
  df$Length = as.numeric(df$Length)
  
  X = length(unique(df$Length))
  Y = sum(abs(range(df$value , na.rm = T)))
  R = X/Y
  
  if ( is.null(LVL) != TRUE ){
    df$Guide = factor(df$Guide, levels = LVL)
  }

  g1 = ggplot(df,aes(x=Length, y = value)) + 
    geom_boxplot(aes(group = Length), fill = "#f0f0f0",outlier.shape=20, outlier.size=-1) + 
    geom_smooth(method = 'loess' , se = TRUE, span = 0.5 , fill = "#d9d9d9") +
    geom_jitter( aes(fill = Guide , color = Guide) , shape = 21, size = 1, width = 0.2 ) +
    theme_classic() + ylab(value) + xlab("guide length [nt]") +  ggtitle(TITLE) + 
    #theme(axis.text.x = element_text(angle = 90, hjust = 0, vjust = 0.5)) + 
    coord_fixed(ratio = R) +
    geom_hline(yintercept=0, linetype="dashed", color = "#252525") +
    scale_x_continuous(breaks=c(seq(15,36,2)))

  g2 = ggplot(df,aes(x=Length, y = value)) + 
    geom_boxplot(aes(group = Length), fill = "#f0f0f0",outlier.shape=20, outlier.size=-1) + 
    geom_smooth(method = 'loess' , aes(  color = Guide),  se = FALSE, span = 0.5 , size = 0.35 ) +
    geom_jitter( aes(fill = Guide , color = Guide) , shape = 21, size = 1, width = 0.2 ) +
    theme_classic() + ylab(value) + xlab("guide length [nt]") +  ggtitle(TITLE) + 
    #theme(axis.text.x = element_text(angle = 90, hjust = 0, vjust = 0.5)) + 
    coord_fixed(ratio = R) +
    geom_hline(yintercept=0, linetype="dashed", color = "#252525")+
    scale_x_continuous(breaks=c(seq(15,36,2)))
  
  if (MORE){
    
    
    g3 = ggplot(df,aes(x=Length, y = value)) + 
      geom_boxplot(aes(group = Length), fill = "#f0f0f0",outlier.shape=20, outlier.size=0.01) + 
      geom_smooth(method = 'loess' , se = TRUE, span = 0.5 , fill = "#d9d9d9") +
      theme_classic() + ylab(value) + xlab("guide length [nt]") +  ggtitle(TITLE) + 
      #theme(axis.text.x = element_text(angle = 90, hjust = 0, vjust = 0.5)) + 
      coord_fixed(ratio = R) +
      geom_hline(yintercept=0, linetype="dashed", color = "#252525") +
      scale_x_continuous(breaks=c(seq(15,36,2)))
    
    return(list(g1,g2,g3))
    
    
  }else{
    return(list(g1,g2))
  }

}



########################## sub-routines end  ########

###### execute ######################################


# Read combined result table
Results = read.delim('./data/CombinedTilingScreenResults.csv' , sep = ',', header = T, stringsAsFactors = F)

# Add GuideLength
Results$GuideLength = sapply( Results$GuideName , extractLength)

# Make GuideNames unique
Results$Guide = paste0(Results$Screen,":",gsub("crRNA","",gsub("crRNA0","",gsub("crRNA00","",Results$Guide))))

# Restrict to single Screens
Screens = split(Results, f = Results$Screen)
GFP = Screens[["GFP"]]
CD46 = Screens[["CD46"]]
CD55 = Screens[["CD55"]]
CD71 = Screens[["CD71"]]

# Scale Data individually
GFP$ScaledCS = scale(GFP$meanCS , center = T)[,1]
CD46$ScaledCS = scale(CD46$meanCS , center = T)[,1]
CD55$ScaledCS = scale(CD55$meanCS , center = T)[,1]
CD71$ScaledCS = scale(CD71$meanCS , center = T)[,1]
All = rbind.data.frame(CD46,CD55,CD71)
CD46$Guide  =  factor(gsub("CD46:","", CD46$Guide ))
CD55$Guide  =  factor(gsub("CD55:","", CD55$Guide ))
CD71$Guide  =  factor(gsub("CD71:","", CD71$Guide ))
Selected = All[ which( All$Guide %in% c( "CD46:100", "CD46:742", "CD55:723", "CD55:805" , "CD55:709" , "CD71:526" , "CD71:565")),]


# set levels
lvls.cd46 =  gsub("CD46:","",c("CD46:100",   "CD46:504",  "CD46:660" , "CD46:688" , "CD46:742",  "CD46:778", "CD46:1390" ,"CD46:1429", "CD46:1573", "CD46:1579")) 
lvls.cd55 =  gsub("CD55:","",c("CD55:245" , "CD55:565",  "CD55:709", "CD55:723",  "CD55:763" , "CD55:805" , "CD55:1316", "CD55:1358", "CD55:1476", "CD55:1481")) 
lvls.cd71 =  gsub("CD71:","",c("CD71:66",  "CD71:518", "CD71:526",  "CD71:565" , "CD71:577" , "CD71:592" ,  "CD71:1275" ,"CD71:1379", "CD71:1607", "CD71:1651")) 
lvls.all = c("CD46:100",   "CD46:504",  "CD46:660" , "CD46:688" , "CD46:742",  "CD46:778", "CD46:1390" ,"CD46:1429", "CD46:1573", "CD46:1579",
             "CD55:245" , "CD55:565",  "CD55:709", "CD55:723",  "CD55:763" , "CD55:805" , "CD55:1316", "CD55:1358", "CD55:1476", "CD55:1481",  
             "CD71:66",  "CD71:518", "CD71:526",  "CD71:565" , "CD71:577" , "CD71:592" ,  "CD71:1275" ,"CD71:1379", "CD71:1607", "CD71:1651") 
lvls.selected =  c( "CD46:100", "CD46:742", "CD55:723", "CD55:805" , "CD55:709" , "CD71:526" , "CD71:565") 



# Plot
plots.cd46 = PlotGuideLengthEffects(x = CD46 , value = "ScaledCS", TITLE = "CD46", LVL = lvls.cd46)
plots.cd55 = PlotGuideLengthEffects(x = CD55 , value = "ScaledCS", TITLE = "CD55", LVL = lvls.cd55)
plots.cd71 = PlotGuideLengthEffects(x = CD71 , value = "ScaledCS", TITLE = "CD71", LVL = lvls.cd71)
plots.all = PlotGuideLengthEffects(x = All , value = "ScaledCS", TITLE = "All" , LVL = lvls.all, MORE = TRUE)
plots.selected = PlotGuideLengthEffects(x = Selected , value = "ScaledCS", TITLE = "Selected" , LVL = lvls.selected)

pdf("./figures/FigS9e_GuideLength.pdf", width = 12, height = 17.5, useDingbats = F)
grid.arrange(grobs = list( plots.selected[[1]],plots.selected[[2]],
                           plots.all[[3]],plots.all[[2]],
                           plots.cd46[[1]],plots.cd46[[2]],
                           plots.cd55[[1]],plots.cd55[[2]],
                           plots.cd71[[1]],plots.cd71[[2]]) , ncol = 2)
dev.off()

# pdf("./figures/FigXX_GuideLength_Combined.pdf", width = 16, height = 5, useDingbats = F)
# grid.arrange(grobs = list( plots.all[[1]],plots.all[[2]]) , ncol = 2)
# dev.off()





